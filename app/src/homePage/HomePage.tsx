import Container from '@material-ui/core/Container';
import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';
import * as React from "react";
import BookList from './BookList';
import Category from "../header/Category";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1
    },
  }),
);
const HomePage = (props: any) => {
  const classes = useStyles();
  return (
        <Container maxWidth="lg" className={classes.root}>
          <Category/>
          <BookList/>
        </Container>


  );
};

export default HomePage;
