import {Grid} from "@material-ui/core";
import * as React from "react";
import {withRouter} from "react-router-dom";
import BookCard from "../bookPage/BookCard";
import {BookContext} from '../contexts/BookContext';
import TableFooter from "@material-ui/core/TableFooter";
import TableRow from "@material-ui/core/TableRow";
import TablePagination from "@material-ui/core/TablePagination";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import {createStyles, makeStyles, Theme, useTheme} from "@material-ui/core/styles";
import IconButton from "@material-ui/core/IconButton";
import LastPageIcon from "@material-ui/icons/LastPage";
import FirstPageIcon from "@material-ui/icons/FirstPage";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import {TablePaginationActionsProps} from "@material-ui/core/TablePagination/TablePaginationActions";
import {isNullOrUndefined} from "util";
import TableCell from "@material-ui/core/TableCell";

const useStyles1 = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexShrink: 0,
            marginLeft: theme.spacing(2.5),
        },
    }),
);

function TablePaginationActions(props: TablePaginationActionsProps) {
    const classes = useStyles1();
    const theme = useTheme();
    const {count, page, rowsPerPage, onChangePage} = props;

    const handleFirstPageButtonClick = (event: React.MouseEvent<HTMLButtonElement>) => {
        onChangePage(event, 0);
    };

    const handleBackButtonClick = (event: React.MouseEvent<HTMLButtonElement>) => {
        onChangePage(event, page - 1);
    };

    const handleNextButtonClick = (event: React.MouseEvent<HTMLButtonElement>) => {
        onChangePage(event, page + 1);
    };

    const handleLastPageButtonClick = (event: React.MouseEvent<HTMLButtonElement>) => {
        onChangePage(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
    };

    return (
        <div className={classes.root}>
            <IconButton
                onClick={handleFirstPageButtonClick}
                disabled={page === 0}
                aria-label="first page"
            >
                {theme.direction === 'rtl' ? <LastPageIcon/> : <FirstPageIcon/>}
            </IconButton>
            <IconButton onClick={handleBackButtonClick} disabled={page === 0} aria-label="previous page">
                {theme.direction === 'rtl' ? <KeyboardArrowRight/> : <KeyboardArrowLeft/>}
            </IconButton>
            <IconButton
                onClick={handleNextButtonClick}
                disabled={page >= Math.ceil(count / rowsPerPage) - 1}
                aria-label="next page"
            >
                {theme.direction === 'rtl' ? <KeyboardArrowLeft/> : <KeyboardArrowRight/>}
            </IconButton>
            <IconButton
                onClick={handleLastPageButtonClick}
                disabled={page >= Math.ceil(count / rowsPerPage) - 1}
                aria-label="last page"
            >
                {theme.direction === 'rtl' ? <FirstPageIcon/> : <LastPageIcon/>}
            </IconButton>
        </div>
    );
}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            width: '100%',
        },
        table: {
            minWidth: 500,
        },
        tableWrapper: {
            overflowX: 'auto',
        },
    }),
);
const BookList = (props: any) => {
    const {books}: any = React.useContext(BookContext);
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(props.rowsPerPage ? props.rowsPerPage : 4);
    const classes = useStyles();
    let itemProRowAmount = 6;

    const handleChangePage = (event: React.MouseEvent<HTMLButtonElement> | null, newPage: number) => {
        setPage(newPage);
    };

    if (!isNullOrUndefined(props.itemProRowAmount)) {
        itemProRowAmount = props.itemProRowAmount;
    }
    const handleChangeRowsPerPage = (
        event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
    ) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    const labelRows = ({from, to, count}: {from: any, to: any, count:any}) => {
        return from - to === -1 ? count * itemProRowAmount + '  Bücher' : to * itemProRowAmount + ' von ' + count * itemProRowAmount + '  Bücher'
    };
    let b = books;

    if (!isNullOrUndefined(props.books) && props.bookListInbox) {
        b = props.books;
    }
    const booksLines = [];
    let index = 0;
    let arr = [];
    for (let i = 0; i < b.length; i++) {
        index++;
        arr.push(b[i]);
        if (i === b.length - 1) {
            booksLines.push(arr.slice());
        } else {
            if (index === itemProRowAmount) {
                booksLines.push(arr.slice());
                arr = [];
                index = 0;
            }
        }
    }


    const uuidv4 = () => {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            const r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    };

    const dataL = booksLines.length;

    return (
        <Table className={classes.table} aria-label="custom pagination table">
            <TableBody>
                {(rowsPerPage > 0
                        ? booksLines.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                        : booksLines
                ).map((a: any) => {
                    return (
                            <Grid key={uuidv4()} container spacing={3}>
                            {a.map((d: any) => {
                                const base64Image = 'data:image/jpg;base64,' + d.img;
                                if (!props.bookListInbox && !props.navigateToBook) {
                                    return (
                                        <Grid key={uuidv4()} item xs={6} sm={4} md={2}>
                                            <BookCard key={d.bookid} bookBzch={d.bzch} img={base64Image}
                                                      bookId={d.bookid}
                                                      description={d.deskription} userOfBook={d.userOfBook}/>
                                        </Grid>
                                    )
                                } else if (props.bookListInbox) {
                                    if (!props.navigateToBook) {
                                        return (
                                            <Grid key={uuidv4()} item xs={6} sm={4} md={4}>
                                                <BookCard key={d.bookid} bookBzch={d.bzch} img={base64Image}
                                                          bookId={d.bookid} book={d}
                                                          description={d.deskription} userOfBook={d.userOfBook}
                                                          bookListInbox={true}/>
                                            </Grid>
                                        )
                                    } else {
                                        return (
                                            <Grid key={uuidv4()} item xs={6} sm={4} md={4}>
                                                <BookCard key={d.bookid} bookBzch={d.bzch} img={base64Image}
                                                          bookId={d.bookid} book={d}
                                                          description={d.deskription} userOfBook={d.userOfBook}
                                                          navigateToBook={true}
                                                          bookListInbox={true}/>
                                            </Grid>
                                        )
                                    }
                                }
                            })}
                        </Grid>
                    )
                })
                }
            </TableBody>


            <TableFooter>
                <TableRow>
                    <TablePagination
                        colSpan={3}
                        rowsPerPageOptions={[1, 2, 3, 4, 5, 6, 7, 8, 9, 10,]}
                        labelDisplayedRows={labelRows}
                        count={(dataL as any)}
                        labelRowsPerPage={"Reihen pro Seite"}
                        rowsPerPage={rowsPerPage}
                        page={page}
                        SelectProps={{
                            native: true,
                        }}
                        onChangePage={handleChangePage}
                        onChangeRowsPerPage={handleChangeRowsPerPage}
                        ActionsComponent={TablePaginationActions}
                    />
                </TableRow>
            </TableFooter>
        </Table>

    )

};
export default withRouter(BookList);
