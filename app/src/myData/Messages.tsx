import { Container, createStyles, Grid, makeStyles, Theme } from "@material-ui/core";
import * as React from "react";
import MessageTable from "./MessageTable";
import MyDataContainer from "./MyDataNavigation";
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      marginBottom: 120
    },
    heading: {
      fontSize: theme.typography.pxToRem(15),
      fontWeight: theme.typography.fontWeightRegular,
    },
    test: {
      display: "block"
    },
    avatar: {
      backgroundColor: "#3aafa9",
    },
  }),
);

const Messages: React.FC = () => {

  const classes = useStyles();
  return (
    <Container maxWidth="lg" className={classes.root}>
      <Grid container>
        <Grid item xs={3}>
          <MyDataContainer value={1}/>
        </Grid>
        <Grid item xs={8}>
          <h1>Meine Nachrichten</h1>
          <MessageTable/>
        </Grid>
      </Grid>
    </Container>
  );
};

export default Messages;
