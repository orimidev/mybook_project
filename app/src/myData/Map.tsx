import React, {useState} from 'react';
import Container from "@material-ui/core/Container";
import ajax from "superagent";
import {makeStyles} from "@material-ui/core/styles";
import PlacesAutocomplete, {geocodeByAddress, getLatLng} from "react-places-autocomplete";
import SearchIcon from '@material-ui/icons/Search';
import {Grid, IconButton, List, ListItem, TextField, Typography} from "@material-ui/core";
import {isNullOrUndefined} from "util";
import {BookContext} from "../contexts/BookContext";
import BookMarker from "./BookMarker";
import GoogleMapReact from "google-map-react";
import ExploreIcon from '@material-ui/icons/Explore';

const useStyles = makeStyles({
    img: {
        paddingRight: "1vw",
        width: "100px",
        height: "130px"
    },
    root: {
        width: '100%',
        maxWidth: 360,
        borderRadius: "8px"
    },
    classFlex: {
        display: "flex",
        lineHeight: "3vh"
    },
    classMain: {
        display: "flex",
    },
    cols: {
        display: "flex",
        flexDirection: "column"
    },
    naviDiv: {
        margin: "2vh 0 2vh 0",
        width: "100%"
    },
    naviScope: {
        marginTop: "10vh",
        display: "flex",
        flexDirection: "column"
    },
    locationSearch: {
        display: "flex",
    }
});

const SimpleMap = (props: any) => {
    const classes = useStyles();
    const [center, setCenter] = useState(!isNullOrUndefined(props.lngCenter) && !isNullOrUndefined(props.latCenter) ?
        {lat: parseFloat(props.latCenter), lng: parseFloat(props.lngCenter)} : {lat: 52.532248, lng: 13.490037});
    const [zoom, setZoom] = useState(14);
    const [ort, setOrt] = useState("testd");
    const [zoomToBook, setZoomToBook] = useState();
    const [adresse, setAdresse] = useState('');
    const [latLng, setLatLng] = useState();
    const [listBooks, setListBooks] = useState();
    const {books, getAllBooks}: any = React.useContext(BookContext);


    // Try HTML5 geolocation.
    const handleAdresseClick = () => {
        if (adresse === "" && isNullOrUndefined(props.latCenter)) {
            setCenter({lat: 52.532248, lng: 13.490037});
        } else {
            ajax.get(" https://maps.googleapis.com/maps/api/geocode/json?address=" + adresse + ",+Germany&key=AIzaSyDsHlrJFZ-0D5w16OY7obr7VfMBMWXX4Ro")
                .end((err: any, res: any) => {
                    const {lat, lng} = res.body.results[0].geometry.location;
                    setCenter({lat, lng});
                });
        }
    };


    const setCenterFunc = (lat: any, lng: any) => {
        setCenter({lat, lng});
    };

    const goToBook = (b: any) => {
        books.forEach((d: any) => {
            if (d === b) {
                setCenter({lat: parseFloat(d.lat), lng: parseFloat(d.lng)})
            }
        });
    };

    const handleBookSelect = (val: any) => {

        if (val.toString() === "" || val.toString() === " ") {
            setListBooks([]);
        } else {
            if (!isNullOrUndefined(val) && val.toString() !== "" && val.toString() !== " ") {

                   const listBooksCurrent: any[] = [];

                books.forEach((d: any) => {
                    if (!isNullOrUndefined(d)) {
                        if(!isNullOrUndefined(d.bzch)) {
                            if (d.bzch.toLowerCase().includes(val.toString().toLowerCase())) {
                                listBooksCurrent.push(d);
                            }
                        }
                    }
                });
                setListBooks(listBooksCurrent);
            }
        }
    };


    const handleSelect = (address: any) => {
        if (isNullOrUndefined(zoomToBook) || zoomToBook === "" || zoomToBook === " ")
            geocodeByAddress(address)
                .then(results => getLatLng(results[0]))
                .then(latLng => {
                    setCenterFunc(latLng.lat, latLng.lng)
                })
                .catch(error => {
                    throw new Error("latlng ist undefined!")
                });
    };


    const handleChange = (address: any) => {
        setLatLng(latLngArr);
        setAdresse(address);
    };


    const latLngArr: any[] = [];
    return (
        <div className={classes.classMain}>
            <Container maxWidth="lg">
                <Grid container spacing={3}>
                    <Grid item xs={3}>
                    <div className={classes.cols}>
                <div className={classes.naviDiv}>
                    <TextField style={{width: "100%"}}
                               onChange={(e) => handleBookSelect(e.target.value)}
                               label={
                                   <div className={classes.classFlex}><SearchIcon/>
                                       <div>Buch...</div>
                                   </div>
                               }
                    />
                </div>
                {!isNullOrUndefined(listBooks) ? <List style={{maxHeight: "50vh", overflow: "auto"}}  className={classes.root}>
                    {listBooks.length > 0 ? listBooks.map((d: any) => {
                        const base64Image = 'data:image/jpg;base64,' + d.img;
                        return (<ListItem button onClick={() => goToBook(d)}>
                            <img className={
                                classes.img
                            } src={base64Image} width="50" height="50"/>
                            <Typography
                                component="span"
                                color="textPrimary"
                            >
                                {d.bzch}
                            </Typography>
                        </ListItem>)
                    }) : null}

                </List> : null}
                <div className={classes.locationSearch}>
                <div className={classes.naviDiv}>
                    <PlacesAutocomplete
                        value={adresse}
                        onChange={handleChange}
                        onSelect={handleSelect}
                    >
                        {({getInputProps, suggestions, getSuggestionItemProps, loading}) => (
                            <div>
                                <TextField style={{width: "100%"}}
                                           {...getInputProps({
                                               className: 'location-search-input',
                                           })}
                                           onKeyPress={(event: any) => {
                                               if (event.key === 'Enter') {
                                                   handleAdresseClick();
                                               }
                                           }}
                                           label={
                                               <div className={classes.classFlex}><SearchIcon/>
                                                   <div>Ort...</div>
                                               </div>
                                           }
                                />
                                <div className="autocompleteDropdownContainer" onMouseOut={ () => setAdresse("")}>
                                    {loading && <div>Loading...</div>}
                                    {suggestions.map(suggestion => {
                                        const className = suggestion.active
                                            ? 'suggestion-item--active'
                                            : 'suggestion-item';
                                        // inline style for demonstration purpose
                                        const style = suggestion.active
                                            ? {
                                                backgroundColor: '#3aafa9',
                                                color: '#ffffff',
                                                cursor: 'pointer'
                                            }
                                            : {
                                                backgroundColor: 'rgba(200,207,218,0.32)',
                                                color: '#000000',
                                                cursor: 'pointer'
                                            };
                                        return (
                                            <div
                                                {...getSuggestionItemProps(suggestion, {
                                                    className,
                                                    style,
                                                })}
                                            >
                                                <span onMouseOver={() => {setAdresse(suggestion.description)}}>{suggestion.description}</span>
                                            </div>
                                        );
                                    })}
                                </div>
                            </div>
                        )}
                    </PlacesAutocomplete>
                </div>
                    <IconButton color="primary"  onClick={handleAdresseClick} >
                        <ExploreIcon/>
                    </IconButton>


            </div>
            </div>
                    </Grid>
                    <Grid item xs={9}>
                    <div style={{height: '80vh', width: '100%'}}>
                    <GoogleMapReact
                        bootstrapURLKeys={{key: 'AIzaSyDsHlrJFZ-0D5w16OY7obr7VfMBMWXX4Ro'}}
                        center={center}
                        defaultZoom={zoom}
                    >
                        {books.map((d: any) => {
                            if (d.lat !== null && d.lng !== null) {
                                const base64Image = 'data:image/jpg;base64,' + d.img;
                                latLngArr.push(d.lat + d.lng);
                                return (
                                    <BookMarker
                                        key={d.bookid}
                                        lat={d.lat}
                                        lng={d.lng}
                                        book={d}
                                        img={base64Image}
                                        color="black"
                                    />)
                            }
                            return null;
                        })}

                    </GoogleMapReact>

                </div>
                    </Grid>
                </Grid>
            </Container>
        </div>
    );
};

export default SimpleMap;







