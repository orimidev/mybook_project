import {
    Avatar,
    Button,
    Container,
    Grid,
    List,
    ListItem,
    ListItemAvatar,
    ListItemText,
    Paper,
    TextField,
    Theme,
    Typography
} from "@material-ui/core";
import * as React from "react";
import MyDataContainer from "./MyDataNavigation";
import {MessageContext} from "../contexts/MessageContext";
import {createStyles, makeStyles} from "@material-ui/styles";
import {isNullOrUndefined} from "util";
import Dialog from "@material-ui/core/Dialog";
import Chip from '@material-ui/core/Chip';
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import BookList from "../homePage/BookList";
import {BookContext} from "../contexts/BookContext";


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        text: {
            padding: theme.spacing(2, 2, 0),
        },
        chips: {
            display: "flex",
            flexWrap: "wrap",
        },
        chipsItem: {
            marginTop: "1vh",
            marginRight: "1vh"
        },
        paper: {
            width: "100%",

        },
        inboxAndBtn: {
        },
        subheader: {
            backgroundColor: theme.palette.background.paper,
        },
        appBar: {
            top: 'auto',
            bottom: 0,
        },
        grow: {
            flexGrow: 1,
        },
        tableWrapper: {
            height: 440,
            overflow: 'auto',
        },
        activUserColor: {
            backgroundColor: '#3aafa9'
        },
        pLink: {
            padding: "1vh 1vh 1vh 1vh"
        },
    }),
);

const MessageDetail: React.FC = (props: any) => {
    const {getMsgBoxById, msgBoxById, chat, bookExchangeMsg, updateBookExchangeMsg}: any = React.useContext(MessageContext);
    const userId = localStorage.getItem('userId');
    const [value, setValue] = React.useState('');
    const {myBooks, getMyBooks, myBookloading, setMyBookLoading}: any = React.useContext(BookContext);
    const [open, setOpen] = React.useState(false);
    const textToSplit = "XUONGDONGNHE";
    const [msgId, setMsgId] = React.useState(props.match.params.id);
    const classes = useStyles();

    React.useEffect(() => {
        getMsgBoxById(msgId);
    });


    React.useEffect(() => {
        if(!isNullOrUndefined(userId) && !isNullOrUndefined(msgBoxById)) {
            if(+msgBoxById.buyer.userId == +userId) {
                getMyBooks(+msgBoxById.seller.userId)
            } else {
                getMyBooks(+msgBoxById.buyer.userId)
            }
        }

    });

    if (isNullOrUndefined(myBooks) && !myBookloading) {
        setMyBookLoading(true);
    }
    const handleClick = () => {
        if (!isNullOrUndefined(userId) && value.trim() !== "") {
            chat(value, msgBoxById, +msgBoxById.buyer.userId == +userId);
        }
        setValue('');
    };

    const handleClickAustausch = () => {
        if (!isNullOrUndefined(bookExchangeMsg)) {
            if (bookExchangeMsg.length > 1) {
                const lastItem = bookExchangeMsg[bookExchangeMsg.length - 1];
                const arrWithOutLastItem: any[] = [];
                for(let i = 0; i< bookExchangeMsg.length-1; i++ ) {
                    if(i !== bookExchangeMsg.length) {
                        arrWithOutLastItem.push(bookExchangeMsg[i]);
                    }
                }
                const booksText =  arrWithOutLastItem.join(', ') + " und " + lastItem;
                setValue("Sind die Bücher " + booksText + " noch zu haben ?")
            } else if (bookExchangeMsg.length === 1) {
                setValue("Ist das Buch " + bookExchangeMsg[0] + " noch zu haben ?")
            }
        }
        handleClose();
    };

    const uuidv4 = () => {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            const r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    };

    const handleClose = () => {
        setOpen(false);

    };

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleDelete = (item: any) => {
        const updateBooks = bookExchangeMsg.filter((d: any) => {
            return d !== item
        });
        updateBookExchangeMsg(updateBooks);
    };


    return (
        <Container maxWidth="lg">
            {!isNullOrUndefined(msgBoxById) && !isNullOrUndefined(userId) ?
                <Grid container>
                    <Grid item xs={3}>
                        <MyDataContainer value={1}/>
                    </Grid>
                    <Grid item xs={8}>
                        <Paper square className={classes.paper}>
                            <div className={classes.inboxAndBtn}>
                                <Typography className={classes.text} variant="h5" gutterBottom>
                                    Inbox
                                </Typography>
                                {/*Buch Austausch PopUp*/}
                                <Dialog
                                    open={open}
                                    onClose={handleClose}
                                    aria-labelledby="form-dialog-title"
                                >
                                    <div>
                                        <DialogTitle id="form-dialog-title">Bücher zum Austauschen
                                            auswählen</DialogTitle>
                                        <DialogContent>
                                            {myBookloading === false ?
                                                <BookList books={myBooks} itemProRowAmount={3} rowsPerPage={5}
                                                          bookListInbox={true}/>
                                                : null}
                                            <div className={classes.chips}>
                                                {
                                                    bookExchangeMsg ? bookExchangeMsg.map((d: any) => {
                                                        return (
                                                            <Chip className={classes.chipsItem} label={d} onDelete={() => handleDelete(d)}
                                                                  color="primary"/>
                                                        )
                                                    }) : null
                                                }
                                            </div>
                                        </DialogContent>
                                        <DialogActions>
                                            <Button onClick={handleClose}>
                                                Zurück
                                            </Button>
                                            <Button onClick={handleClickAustausch} color="primary">
                                                Austausch Anfragen
                                            </Button>
                                        </DialogActions>
                                    </div>
                                </Dialog>


                                {/* End Buch Austausch PopUp*/}

                            </div>

                            <List>
                                <div className={classes.tableWrapper}>
                                    {msgBoxById.msg.split(textToSplit).map((d: any) => {
                                        return (
                                            <ListItem key={uuidv4()}>
                                                <ListItemAvatar>
                                                    {((+msgBoxById.buyer.userId == +userId && d.startsWith('buyer:')) || (+msgBoxById.seller.userId == +userId && d.startsWith('seller:'))) ?
                                                        <Avatar className={classes.activUserColor}>
                                                            {d.startsWith('buyer:') ?
                                                                msgBoxById.buyer.firstName.charAt(0) : msgBoxById.seller.firstName.charAt(0)}
                                                        </Avatar> : <Avatar>
                                                            {d.startsWith('buyer:') ?
                                                                msgBoxById.buyer.firstName.charAt(0) : msgBoxById.seller.firstName.charAt(0)}
                                                        </Avatar>
                                                    }
                                                </ListItemAvatar><ListItemText
                                                secondary={d.replace("buyer:", "").replace("seller:", "")}/>
                                            </ListItem>
                                        )
                                    })}
                                </div>
                                <Typography className={classes.text} onClick={handleClickOpen} gutterBottom>
                                    <Button style={{width:"100%"}} color="secondary" className={classes.pLink}> Angebot von
                                        { +msgBoxById.buyer.userId == +userId ?" " + msgBoxById.seller.firstName :" " + msgBoxById.buyer.firstName}</Button>
                                </Typography>
                                <TextField
                                    autoFocus
                                    margin="normal"
                                    placeholder="private Nachricht..."
                                    fullWidth
                                    multiline
                                    variant="outlined"
                                    rows="3"
                                    value={value}
                                    onChange={(e) => setValue(e.target.value)}
                                    onKeyPress={event => {
                                        if (event.key === 'Enter') {
                                            handleClick()
                                        }
                                    }}
                                />
                            </List>
                        </Paper>
                    </Grid>
                </Grid> : null}
        </Container>
    );
};

export default MessageDetail;
