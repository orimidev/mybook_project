import {Container, Grid, makeStyles} from "@material-ui/core";
import * as React from "react";
import MyDataContainer from "./MyDataNavigation";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import {UserDataContext} from "../contexts/UserDataContext";
import {isNullOrUndefined} from "util";
import ProfilEdit from "../bookPage/ProfilEdit";

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(3, 2),
    },
    btn: {
        marginBottom: "2vh"
    }
}));
const Profile: React.FC = () => {
    const classes = useStyles();

    const {findUserById, user}: any = React.useContext(UserDataContext);
    const userId = localStorage.getItem('userId');

    if (!isNullOrUndefined(userId) && isNullOrUndefined(user)) {
        findUserById(userId);
    }


    return (
        <Container maxWidth="lg">
            <Grid container>
                <Grid item xs={3}>
                    <MyDataContainer value={2}></MyDataContainer>
                </Grid>
                <Grid item xs={8}>
                    <h1>Mein Profil</h1>
                    <Paper className={classes.root}>
                        {isNullOrUndefined(user) ? <Typography variant="h5" component="h3">
                                ...
                            </Typography> :
                            <div>
                                <Typography variant="h5" component="h3">
                                    {user.firstName + " " + user.lastName}
                                </Typography>
                                <Typography component="p">
                                    Benutzername: {user.userName}
                                </Typography>
                                <Typography component="p">
                                    Password:
                                    *********
                                </Typography>
                                <Typography component="p">
                                    {!isNullOrUndefined(user.address) ? 'Adresse: ' + user.address.strasse + ', ' + user.address.plz + ' ' + user.address.city.bzch : ''}
                                </Typography>
                                <div className={classes.btn}>
                                    <ProfilEdit user={user}/>
                                </div>
                            </div>}
                    </Paper>
                </Grid>
            </Grid>

        </Container>
    );
};

export default Profile;
