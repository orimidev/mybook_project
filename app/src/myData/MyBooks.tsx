import * as React from "react";
import {Container, Grid} from "@material-ui/core";
import {BookContext} from "../contexts/BookContext";
import {UserDataContext} from "../contexts/UserDataContext";
import BookCard from "../bookPage/BookCard";
import MyDataContainer from "./MyDataNavigation";

const MyBooks: React.FC = () => {
    const { myBooks, getMyBooks, myBookloading }: any = React.useContext(BookContext);
    const { userId }: any = React.useContext(UserDataContext);
    React.useEffect(() => {
        getMyBooks(userId)
    });
    return (
        <Container maxWidth="lg">
            <Grid container>
                <Grid item xs={3}>
                    <MyDataContainer value={0}/>
                </Grid>
                <Grid item xs={8}>
                    <h1>Meine Bücher zum Angebot</h1>

                    {myBookloading === false ? <Grid container spacing={3}>
                        {myBooks.map((d: any) => {

                            const base64Image = 'data:image/jpg;base64,' + d.img;
                            return (
                                <Grid key={d.bookid} item xs={6} sm={4} md={3}>
                                    <BookCard bookBzch={d.bzch} img={base64Image} userOfBook={d.userOfBook} bookId={d.bookid} description={d.deskription}/>
                                </Grid>

                            )
                        }

                        )}
                    </Grid> : null}
                </Grid>
            </Grid>
        </Container>
    );
};

export default MyBooks;
