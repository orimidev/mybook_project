import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import TextField from "@material-ui/core/TextField";
import * as React from "react";
import {withRouter} from "react-router-dom";
import {MessageContext} from "../contexts/MessageContext";

function MessagePopup(props: any) {
    const [open, setOpen] = React.useState(false);
    const [value, setValue] = React.useState('');
    const {addMsg}: any = React.useContext(MessageContext);

    const handleClose = () => {
        setOpen(false);
    };

    const handleClickOpen = () => {
        setOpen(true);
    };
    const handleClick = () => {
        addMsg('XUONGDONGNHE' + localStorage.getItem('user') + ' : ' + value, props.messageBox, props.isBuyer);
        handleClose();
    };
    return (
        <div>
            <Button variant="outlined" onClick={handleClickOpen}>
                Antworten
            </Button>
            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="form-dialog-title"
            >
                <div>
                    <DialogTitle id="form-dialog-title">Nachricht schreiben</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            Bitte melden Sie sich an, um alle Funktionen nutzen zu können, oder
                        </DialogContentText>
                        <TextField
                            margin="normal"
                            placeholder="Private Nachricht ..."
                            fullWidth
                            multiline
                            variant="outlined"
                            rows="4"
                            onChange={(e) => setValue(e.target.value)}
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleClose}>
                            Cancel
                        </Button>
                        <Button onClick={handleClick} color="primary">
                            Senden
                        </Button>
                    </DialogActions>
                </div>
            </Dialog>

        </div>
    );
}

export default withRouter(MessagePopup)