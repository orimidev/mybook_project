import React from 'react';
import './Marker.css';
import {Theme, withStyles} from "@material-ui/core";
import MuiDialogContent from "@material-ui/core/DialogContent/DialogContent";
import Dialog from "@material-ui/core/Dialog";
import BookList from "../homePage/BookList";
import {BookContext} from "../contexts/BookContext";
import BookCard from "../bookPage/BookCard";


const DialogContent = withStyles((theme: Theme) => ({
    root: {
        padding: theme.spacing(2),
    },
}))(MuiDialogContent);

const BookMarker = (props: any) => {
    const {books}: any = React.useContext(BookContext);
    const {book, img} = props;
    const [booksToShow, setBooksToShow] = React.useState();
    const [open, setOpen] = React.useState(false);
    const handleClickOpen = () => {
        setOpen(true);
    };
    const handleClose = () => {
        setOpen(false);
    };


    return (
        <div>
            <div onClick={handleClickOpen}>
                <div
                    className="pin bounce"
                    style={{backgroundColor: 'black', cursor: 'pointer'}}
                    title={book.bzch}
                />
                <div className="pulse"/>
            </div>

            <Dialog onClose={handleClose} aria-labelledby="customized-dialog-title" open={open}>
                <DialogContent>
                    {
                        books.filter((d: any) => {
                            return d.lat == book.lat && d.lng == book.lng
                        }).length === 1 ?
                            <BookCard map={true} bookBzch={book.bzch} img={img} bookId={book.bookid}
                                      description={book.deskription} userOfBook={book.userOfBook}/>
                            : <BookList
                                navigateToBook={true}
                                books={books.filter((d: any) => {
                                    return d.lat == book.lat && d.lng == book.lng
                                })} itemProRowAmount={3} rowsPerPage={5}
                                bookListInbox={true}/>
                    }

                </DialogContent>
            </Dialog>

        </div>
    );
};

export default BookMarker;
