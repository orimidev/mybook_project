import { Container, Tab, Tabs, Theme } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import * as React from "react";

function a11yProps(index: any) {
  return {
    id: `vertical-tab-${index}`,
    'aria-controls': `vertical-tabpanel-${index}`,
  };
}
const useStyles = makeStyles((theme: Theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
    display: 'flex',
    height: 224,
  },
  tabs: {
    borderRight: `1px solid ${theme.palette.divider}`,
  },
}));

const MyDataContainer = (props: any) => {
  const classes = useStyles();
  const [value, setValue] = React.useState(props.value);

  const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    setValue(newValue);
  };
  return (
    <Container maxWidth="lg">
      <div className={classes.root}>
        <Tabs
          orientation="vertical"
          variant="scrollable"
          value={value}
          onChange={handleChange}
          aria-label="Vertical tabs example"
          className={classes.tabs}
        >
          <Tab label="Meine Bücher" href={"/mybooks"} {...a11yProps(0)} />
          <Tab label="Nachrichten" href={"/messages"} {...a11yProps(1)} />
          <Tab label="Mein Profile" href={"/profile"}{...a11yProps(2)} />
        </Tabs>
      </div>
    </Container>
  );
};

export default MyDataContainer;
