import {
    Avatar,
    Card,
    CardActionArea,
    CardMedia,
    List,
    ListItem,
    ListItemAvatar,
    ListItemText,
    Tooltip
} from '@material-ui/core';
import IconButton from '@material-ui/core/IconButton';
import Paper from '@material-ui/core/Paper';
import {createStyles, makeStyles, Theme, useTheme} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import FirstPageIcon from '@material-ui/icons/FirstPage';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import LastPageIcon from '@material-ui/icons/LastPage';
import React from 'react';
import {withRouter} from "react-router-dom";
import {MessageContext} from '../contexts/MessageContext';
import {isNullOrUndefined} from "util";

const useStyles1 = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexShrink: 0,
            marginLeft: theme.spacing(2.5),
        },
    }),
);

interface TablePaginationActionsProps {
    count: number;
    page: number;
    rowsPerPage: number;
    onChangePage: (event: React.MouseEvent<HTMLButtonElement>, newPage: number) => void;
}

 function TablePaginationActions(props: TablePaginationActionsProps) {
    const classes = useStyles1();
    const theme = useTheme();
    const { count, page, rowsPerPage, onChangePage } = props;

    const handleFirstPageButtonClick = (event: React.MouseEvent<HTMLButtonElement>) => {
        onChangePage(event, 0);
    };

    const handleBackButtonClick = (event: React.MouseEvent<HTMLButtonElement>) => {
        onChangePage(event, page - 1);
    };

    const handleNextButtonClick = (event: React.MouseEvent<HTMLButtonElement>) => {
        onChangePage(event, page + 1);
    };

    const handleLastPageButtonClick = (event: React.MouseEvent<HTMLButtonElement>) => {
        onChangePage(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
    };

    return (
        <div className={classes.root}>
            <IconButton
                onClick={handleFirstPageButtonClick}
                disabled={page === 0}
                aria-label="first page"
            >
                {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
            </IconButton>
            <IconButton onClick={handleBackButtonClick} disabled={page === 0} aria-label="previous page">
                {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
            </IconButton>
            <IconButton
                onClick={handleNextButtonClick}
                disabled={page >= Math.ceil(count / rowsPerPage) - 1}
                aria-label="next page"
            >
                {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
            </IconButton>
            <IconButton
                onClick={handleLastPageButtonClick}
                disabled={page >= Math.ceil(count / rowsPerPage) - 1}
                aria-label="last page"
            >
                {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
            </IconButton>
        </div>
    );
}


const useStyles2 = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            width: '100%',
        },
        table: {
            minWidth: 500,
        },
        tableWrapper: {
            overflowX: 'auto',
        },
        activMsg: {
            borderLeft: "4px solid #3aafa9",
            color: "#35a69f",
        },
        unactivMsg: {
            borderLeft: "0px solid #3aafa9",
            background: "rgba(0, 0, 0, .03)",
        },
        card: {
            height: 100,
            width: 60,
            padding: 0
        },
        media: {
            height: 100,
            width: 60,
        },
    }),
);

function MessageTable(props: any) {
    const classes = useStyles2();
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(5);
    const { msg, loading, removeMsg, readMsg }: any = React.useContext(MessageContext);
    const userId = localStorage.getItem('userId');
    const msgL = localStorage.getItem('msgLength');

    const handleChangePage = (event: React.MouseEvent<HTMLButtonElement> | null, newPage: number) => {
        setPage(newPage);
    };


    const labelRows = ({from, to, count}: {from: any, to: any, count: any}) => {
        return from - to === -1 ? count  + '  Nachrichten' : to  + ' von ' + count  + '  Nachrichten'
    };
    const handleChangeRowsPerPage = (
        event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
    ) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };
    const handleClick = (msgBoxId: any, msgBox: any, isBuyer: boolean) => {
        readMsg(msgBox, isBuyer);
        props.history.push('/message/' + msgBoxId);
    };


    const timeConverter = (timestamp: any) => {
        const a = new Date(timestamp);
        const year = a.getFullYear();
        const month = a.getMonth() + 1;
        const date = a.getDate();
        return date + '.' + month + '.' + year;
    };
    return (
        <Paper className={classes.root}>
            <div className={classes.tableWrapper}>
                <Table className={classes.table} aria-label="custom pagination table">
                    {loading === false ?
                        <TableBody>
                            {(rowsPerPage > 0
                                ? msg.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                : msg
                            ).map((d: any) => {
                                const base64Image = 'data:image/jpg;base64,' + d.book.img;
                                return (
                                    <TableRow key={d.msgBoxId}
                                        hover
                                        className={d.newmsgForSeller == 1 && d.seller.userId == userId
                                            || d.newmsgForBuyer == 1 && d.buyer.userId == userId ? classes.activMsg : classes.unactivMsg}>
                                        <TableCell colSpan={6}  onClick={() => handleClick(d.msgBoxId, d, d.buyer.userId == userId)}>
                                            <List>
                                                <ListItem>
                                                    <ListItemAvatar>
                                                        <Avatar>
                                                            {d.buyer.userId == userId ? d.seller.firstName.charAt(0) : d.buyer.firstName.charAt(0)}
                                                        </Avatar>
                                                    </ListItemAvatar>
                                                    <ListItemText primary={d.buyer.userId == userId ? d.seller.firstName : d.buyer.firstName} secondary={timeConverter(d.timestamp)} />
                                                </ListItem>
                                            </List>
                                        </TableCell>
                                        <TableCell >
                                            <Card className={classes.card}>
                                                <CardActionArea>
                                                    <CardMedia
                                                        className={classes.media}
                                                        image={base64Image}
                                                    />
                                                </CardActionArea>
                                            </Card>
                                        </TableCell>
                                        <TableCell >
                                            <Tooltip title="Löschen">
                                                <IconButton onClick={()=> removeMsg(d.msgBoxId)}>
                                                    <DeleteForeverIcon/>
                                                </IconButton>
                                            </Tooltip>
                                        </TableCell>
                                    </TableRow>
                                )
                            })}
                        </TableBody> : null}
                    <TableFooter>
                        <TableRow>
                            <TablePagination
                                colSpan={3}
                                rowsPerPageOptions={[5, 10, 25,]}
                                count={!isNullOrUndefined(msgL)?parseFloat(msgL): 0}
                                rowsPerPage={rowsPerPage}
                                labelRowsPerPage={"Nachrichten pro Seite"}
                                labelDisplayedRows={labelRows}
                                page={page}
                                SelectProps={{
                                    native: true,
                                }}
                                onChangePage={handleChangePage}
                                onChangeRowsPerPage={handleChangeRowsPerPage}
                                ActionsComponent={TablePaginationActions}
                            />
                        </TableRow>
                    </TableFooter>
                </Table>
            </div>
        </Paper>
    );
}
export default withRouter(MessageTable)