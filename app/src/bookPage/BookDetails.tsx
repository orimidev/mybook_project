import {Grid, IconButton, SvgIcon} from "@material-ui/core";
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardMedia from '@material-ui/core/CardMedia';
import {makeStyles} from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import * as React from "react";
import {withRouter} from "react-router-dom";
import BookEdit from "./BookEdit";
import MessageBox from "./MessageBox";
import {CategoryProvider} from "../contexts/CategoryContext";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import SimpleMap from "../myData/Map";
import RoomOutlinedIcon from '@material-ui/icons/RoomOutlined';


const useStyles = makeStyles({
    card: {
        height: 550,
        padding: 0
    },
    media: {
        height: 550,
    },
    description: {
        height: 240,
        width: 600
    },
    content: {
        margin: 15
    },
    dialog: {
        width: "800px",
        height: "100%"
    }
});

function BookDetail(props: any) {
    const [open, setOpen] = React.useState(false);
    const handleClose = () => {
        setOpen(false);
    };

    const handleClickOpen = () => {
        setOpen(true);
    };

    const classes = useStyles();
    const book = props.book;
    let myBook = false;
    if (book.userOfBook.userId.toString() === localStorage.getItem('userId')) {
        myBook = true;
    }

    return (

            <Grid container spacing={3}>
                <Grid item xs={12} sm={4}>
                    <Card className={classes.card}>
                        <CardActionArea>
                            <CardMedia
                                className={classes.media}
                                image={props.img}
                            />
                        </CardActionArea>
                    </Card>
                </Grid>
                <Grid item sm={7} className={classes.content}>
                    <Typography>{book.userOfBook.firstName} {book.userOfBook.lastName}.</Typography>
                    <Typography variant="h4">{book.bzch}</Typography>
                    <Typography variant="h6">{'von ' + book.author}</Typography>
                    <Typography className={classes.description}>{book.deskription}</Typography>
                    <div style={{display: 'flex'}}>
                    <Typography onClick={handleClickOpen}>{book.plz} {book.city.bzch}    <IconButton aria-label="go to" onClick={handleClickOpen} >
                        <RoomOutlinedIcon  style={{color: "#000000", backgroundColor: "#FDCE4B", borderRadius: "40px"}}>
                        </RoomOutlinedIcon>
                    </IconButton></Typography>


                    </div>

                    <Dialog
                        open={open}
                        onClose={handleClose}
                        aria-labelledby="form-dialog-title"
                        maxWidth="lg"
                    >
                        <DialogContent  className={classes.dialog}>
                            <SimpleMap latCenter={book.lat} lngCenter = {book.lng}/>
                        </DialogContent>
                    </Dialog>
                    {myBook ?
                        <CategoryProvider><BookEdit book={book}/></CategoryProvider>
                        : <MessageBox firstName={book.userOfBook.firstName} sellerId={book.userOfBook.userId}
                                      book={book}/>
                    }
                </Grid>
            </Grid>


    );
}

export default withRouter(BookDetail);
