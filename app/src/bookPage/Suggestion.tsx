import * as React from "react";
import { Grid } from "@material-ui/core";
import BookCard from "./BookCard";
import { BookContext } from '../contexts/BookContext';
const Suggestion = (props: any) => {
    const { userBooks, getUserBooks, userloading }: any = React.useContext(BookContext)
    React.useEffect(() => {
        getUserBooks(props.userId, props.bookId)
    })

    return (
        <div>
            <h3>Weitere Bücher von {props.firstName}</h3>

            {userloading === false ? <Grid container spacing={3}>
                {userBooks.map((d: any) => {
                    //   console.log("suggestionBooks", userloading)
                    const base64Image = 'data:image/jpg;base64,' + d.img
                    return (
                        <Grid key={d.bookid} item xs={6} sm={4} md={2}>
                            <BookCard bookBzch={d.bzch} img={base64Image} userOfBook={d.userOfBook} bookId={d.bookid} description={d.deskription}></BookCard>
                        </Grid>
                    )
                }

                )}
            </Grid> : null}

        </div>
    );
};

export default Suggestion;