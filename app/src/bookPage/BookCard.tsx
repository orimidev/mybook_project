import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import {makeStyles} from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import * as React from "react";
import {withRouter} from "react-router-dom";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import {Button, Grid} from "@material-ui/core";
import DialogActions from "@material-ui/core/DialogActions";
import PlaceIcon from "@material-ui/icons/Place";
import {MessageContext} from "../contexts/MessageContext";

const useStyles = makeStyles({
    card: {
        height: 370,
    },
    content: {
        margin: 15
    },
    media: {
        height: 270,

    },
    card2: {
        height: 270,
        width: 150
    },
    media2: {
        height: 270,
        width: 150
    },
    title: {
        fontSize: "18px",
        fontWeight: 600,
        display: "block",
        overflow: "hidden",
        maxHeight: "1.8em",
        wordWrap: "break-word",
        textOverflow: "ellipsis"
    },
    descripton: {
        fontSize: "14px",
        display: "block",
        wordWrap: "break-word",
        textOverflow: "ellipsis"
    },
});

const useStylesForMap = makeStyles({
    card: {
        height: "45vh",
        width: "15vw"
    },
    media: {
        height: "35vh",
        width: "15vw"
    },
});


function BookCard(props: any) {
    const classes = useStyles();
    const classesForMap = useStylesForMap();
    const {setMultiBookExchangeText}: any = React.useContext(MessageContext);

    const [open, setOpen] = React.useState(false);
    /**
     * navigiert zur BookDetailsPage, welche das ausgewählte Buch angezeigt,
     * übergibt userOfBook: der user, dem das Book gehört
     */
    const handleClick = () => {
        props.history.push('/book/' + props.bookId, props.userOfBook);
        window.scrollTo({
            top: 0,
            behavior: 'smooth'
        });
    };

    const handleAuswaehlenClick = () => {
        if(props.navigateToBook) {
            handleClick();
        } else {
            setMultiBookExchangeText("'" + props.bookBzch + "'");
        }

        handleClose();
    };

    const handleClose = () => {
        setOpen(false);
    };

    const handleClickOpen = () => {
        setOpen(true);
    };


    return (
        <Card className={!props.map ? classes.card : classesForMap.card}>
            {!props.bookListInbox ?
                <CardActionArea onClick={handleClick}>
                    <CardMedia
                        className={!props.map ? classes.media : classesForMap.media}
                        image={props.img}
                        title={props.bookBzch}
                    />
                    <CardContent>
                        <Typography className={classes.title} variant="h6" gutterBottom>
                            {props.bookBzch}
                        </Typography>
                        <Typography className={classes.descripton} variant="caption" color="textSecondary"
                                    component="p">
                            {props.description}
                        </Typography>
                    </CardContent>
                </CardActionArea>
                :
                <CardActionArea onClick={handleClickOpen}>
                    <CardMedia
                        className={!props.map ? classes.media : classesForMap.media}
                        image={props.img}
                        title={props.bookBzch}
                    />
                    <CardContent>
                        <Typography className={classes.title} variant="h6" gutterBottom>
                            {props.bookBzch}
                        </Typography>
                        <Typography className={classes.descripton} variant="caption" color="textSecondary"
                                    component="p">
                            {props.description}
                        </Typography>
                    </CardContent>
                </CardActionArea>
            }

            {/*Book für Austausch PopUp*/}
            {props.book ?
                <Dialog open={open}
                        onClose={handleClose}
                        aria-labelledby="form-dialog-title"
                >
                    <div>
                        <DialogContent className={classes.content}>
                            <div>
                                <Grid container spacing={3}>
                                    <Grid item xs={6} sm={4}>
                                        <Card className={classes.card2}>
                                            <CardActionArea>
                                                <CardMedia
                                                    className={classes.media2}
                                                    image={props.img}
                                                />
                                            </CardActionArea>
                                        </Card>
                                    </Grid>
                                    <Grid item xs={6} sm={7}>
                                        {/*<Typography>{props.book.userOfBook.firstName} {props.book.userOfBook.lastName}.</Typography>*/}
                                        <Typography variant="h4">{props.book.bzch}</Typography>
                                        <Typography variant="h6">{'von ' + props.book.author}</Typography>
                                        <Typography className={classes.descripton}>{props.book.deskription}</Typography>
                                        <PlaceIcon/>
                                        <Typography>{props.book.plz} {props.book.city.bzch}</Typography>
                                    </Grid>
                                </Grid>

                            </div>
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={handleClose}>
                                Zurück
                            </Button>
                            <Button onClick={handleAuswaehlenClick} color="primary">
                                Auswählen
                            </Button>
                        </DialogActions>
                    </div>
                </Dialog>

                :
                null
            }


            {/*End Book für Austausch PopUp*/}
        </Card>

    );
}

export default withRouter(BookCard);
