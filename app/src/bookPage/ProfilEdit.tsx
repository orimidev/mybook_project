import {Grid, MenuItem} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import TextField from "@material-ui/core/TextField";
import * as React from "react";
import {withRouter} from "react-router-dom";
import {BookContext} from "../contexts/BookContext";
import {UserDataContext} from "../contexts/UserDataContext";
import {makeStyles} from "@material-ui/core/styles";
import {isNullOrUndefined} from "util";

const useStyle = makeStyles({
    updateBtn: {
        float: "right",

    }
});

function ProfilEdit(props: any) {

    const classes = useStyle();
    const {updateUser}: any = React.useContext(UserDataContext);

    const [userName, setUserName] = React.useState(props.user.userName);
    const [firstName, setFirstName] = React.useState(props.user.firstName);
    const [lastName, setLastName] = React.useState(props.user.lastName);
    const [password, setPassword] = React.useState(props.user.password);
    const [strasse, setStrasse] = React.useState(!isNullOrUndefined(props.user.address) ? props.user.address.strasse : '');
    const [plz, setPlz] = React.useState(!isNullOrUndefined(props.user.address) ? props.user.address.plz : '');
    const [city, setCity] = React.useState(!isNullOrUndefined(props.user.address) ? props.user.address.city.bzch : '');
    const [open, setOpen] = React.useState(false);
    const {cities}: any = React.useContext(BookContext);

    const handleClose = () => {
        setOpen(false);
    };

    const handleClickOpen = () => {
        setOpen(true);
    };


    const handleUpload = (e: any) => {
        e.preventDefault();
        let userToUpdate = props.user;
        let adresseToUpdate: any;
        if (!isNullOrUndefined(props.user.address)) {
            adresseToUpdate = props.user.address;
        } else {adresseToUpdate = {}}
        userToUpdate.userName = userName;
        userToUpdate.firstName = firstName;
        userToUpdate.lastName = lastName;
        userToUpdate.password = password;

        adresseToUpdate.strasse = strasse;
        adresseToUpdate.plz = plz;

        cities.forEach((e: any) => {
            if (e.bzch == city) {
                adresseToUpdate.city = e;
            }
        });
        updateUser(userToUpdate, adresseToUpdate);
        handleClose();
    };


    return (
        <div>
            <Button className={classes.updateBtn} color="primary" onClick={handleClickOpen}>
                Bearbeiten
            </Button>
            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="form-dialog-title"
                maxWidth="lg"
            >
                <div>
                    <DialogTitle id="form-dialog-title">Bearbeiten</DialogTitle>
                    <DialogContent>
                        <form>
                            <TextField
                                label="Vorname"
                                fullWidth
                                margin="normal"
                                variant="outlined"
                                value={firstName}
                                onChange={(e) => setFirstName(e.target.value)}
                            />

                            <TextField
                                label="Nachname"
                                fullWidth
                                margin="normal"
                                variant="outlined"
                                value={lastName}
                                onChange={(e) => setLastName(e.target.value)}
                            />
                            <TextField
                                label="Benutzername"
                                margin="normal"
                                fullWidth
                                variant="outlined"
                                value={userName}
                                onChange={(e) => setUserName(e.target.value)}
                            />
                            <TextField
                                label="Password"
                                margin="normal"
                                fullWidth
                                variant="outlined"
                                value={password}
                                type="password"
                                onChange={(e) => setPassword(e.target.value)}
                            />
                            <TextField
                                label="Straße und Hausnummer"
                                margin="normal"
                                fullWidth
                                variant="outlined"
                                value={strasse}
                                onChange={(e) => setStrasse(e.target.value)}
                            />
                            <Grid item xs={9}>
                                <TextField
                                    select
                                    value={city}
                                    margin="normal"
                                    label="Stadt"
                                    fullWidth
                                    variant="outlined"
                                    onChange={(e) => setCity(e.target.value)}
                                >
                                    {cities.map((option: any) => (
                                        <MenuItem key={option.idCity} value={option.bzch}>
                                            {option.bzch}
                                        </MenuItem>
                                    ))}
                                </TextField>
                            </Grid>

                        </form>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleClose}>
                            Cancel
                        </Button>
                        <Button color="primary" onClick={handleUpload}>
                            Änderung sichern
                        </Button>
                    </DialogActions></div>
            </Dialog>

        </div>

    );
}

export default withRouter(ProfilEdit)