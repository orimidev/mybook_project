import {Grid, MenuItem} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import TextField from "@material-ui/core/TextField";
import * as React from "react";
import {withRouter} from "react-router-dom";
import ImageUploader from 'react-images-upload';
import {isNullOrUndefined} from "util";
import {BookContext} from "../contexts/BookContext";
import {CategoryContext} from '../contexts/CategoryContext';
import {makeStyles} from "@material-ui/styles";
import ajax from "superagent";

const useStyles = makeStyles({

    btn: {
        margin: '1vh 2vw 0 0'
    }
});

function BookEdit(props: any) {
    const [open, setOpen] = React.useState(false);
    const [popOpen, setPopOpen] = React.useState(false);
    const [editDisabeld, setEditDisabeld] = React.useState(false);
    const {removeBook, editBook}: any = React.useContext(BookContext);
    const [lat, setLat] = React.useState();
    const [lng, setLng] = React.useState();
    const book = props.book;
    const {addBook, cities}: any = React.useContext(BookContext);
    const {categories}: any = React.useContext(CategoryContext);
    const [title, setTitle] = React.useState(book.bzch);
    const [author, setAuthor] = React.useState(book.author);
    const [description, setDescription] = React.useState(book.deskription);
    const [catagory, setCatagory] = React.useState(book.katagory.bzch);
    const [city, setCity] = React.useState(props.book.city.bzch);
    const [plz, setPlz] = React.useState(props.book.plz);
    const [img, setImg] = React.useState(book.img);
    const [errorTitle, setErrorTitle] = React.useState();
    const [errorDescription, setErrorDescripton] = React.useState();
    const [error, setError] = React.useState(false);
    const [iserror, setisError] = React.useState(false);

    const [imagePreviewTitle, setImagePreviewTitle] = React.useState("Maximale Größe: 5mb, Format: jpg, gif, png, jpeg");
    const classes = useStyles();
    const handleClose = () => {
        setOpen(false);
    };

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleDeletePopClose = () => {
        setPopOpen(false);
    };

    const handleDeletePopClickOpen = () => {
        setPopOpen(true);
    };


    const handleRemoveClick = () => {
        removeBook(book.bookid);
        props.history.push('/');
    };

    const handleUpload = (e: any) => {
        e.preventDefault();
        const newBook = props.book;
        newBook.bzch = title;
        newBook.deskription = description;
        newBook.author = author;
        newBook.img = img;
        newBook.plz = plz;
        cities.forEach((e: any) => {
            if (e.bzch == city) {
                newBook.city = e;
            }
        });
        categories.forEach((e: any) => {
            if (e.bzch == catagory) {
                newBook.katagory = e;
            }
        });
      if(!isNullOrUndefined(city) && !isNullOrUndefined(plz)) {
          onChangePlzOrCity(newBook);
      } else {
         alert("Stadt oder Plz fehlt!!!")
      }
      





    };

    const adressChange = (key: string, val: any) => {
        switch (key) {
            case "city":
                setCity(val);
                break;
            case "adress":
                setPlz(val);
                break;
        }
    };

    const onChangePlzOrCity = (newBook: any) => {

        setOpen(false);
        setEditDisabeld(true);
        ajax.get(" https://maps.googleapis.com/maps/api/geocode/json?address=" + plz + "," + city.bzch + ",Deutschland&key=AIzaSyDsHlrJFZ-0D5w16OY7obr7VfMBMWXX4Ro")
            .end((err: any, res: any) => {
                if(!isNullOrUndefined(res.body.results)) {
                    const {lat, lng} = res.body.results[0].geometry.location;
                    setLat(lat);
                    setLng(lng);
                    if (!isNullOrUndefined(lat)) {
                        newBook.lat = lat.toString();
                    }
                    if (!isNullOrUndefined(lng)) {
                        newBook.lng = lng.toString();
                    }
                    editBook(newBook);
                    setEditDisabeld(false);
                }

            });
    };

    const onChangeImg = (file: any) => {
        getBase64(file[0], (result: any) => {
            setImagePreviewTitle(file[0].name);
            setImg(result.replace('data:image/png;base64,', '')
                .replace('data:image/jpeg;base64,', '')
                .replace('data:image/jpg;base64,', '')
                .replace('data:image/gif;base64,', ''));
        });

    };
    const getBase64 = (file: any, cb: any) => {
        if (!isNullOrUndefined(file)) {
            let reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function () {
                cb(reader.result)
            };
            reader.onerror = function (error) {
                console.log('Error: ', error);
            };
        } else return null;

    };

    return (
        <div>
            <Button className={classes.btn} color="secondary" onClick={handleDeletePopClickOpen}>
                Löschen
            </Button>
            <Dialog
                open={popOpen}
                onClose={handleDeletePopClose}
                aria-labelledby="form-dialog-title"
                maxWidth="lg"
            >
                <div>
                    <DialogTitle id="form-dialog-delete">Löschen</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            Möchten Sie das Buch {title} aus dem System entfernen ?
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleDeletePopClose}>
                            Nein
                        </Button>
                        <Button color="primary" onClick={handleRemoveClick}>
                            Ja
                        </Button>
                    </DialogActions>
                </div>

            </Dialog>

            <Button color="primary" className={classes.btn} onClick={handleClickOpen}>
                Bearbeiten
            </Button>
            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="form-dialog-title"
                maxWidth="lg"
            >
                <div>
                    <DialogTitle id="form-dialog-title">Bearbeiten</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            Bitte melden Sie sich an, um alle Funktionen nutzen zu können, oder
                        </DialogContentText>
                        <form>
                            <ImageUploader
                                withIcon={true}
                                buttonText='Photo hinzufügen'
                                onChange={onChangeImg}
                                imgExtension={['.jpg', '.jpeg', '.gif', '.png']}
                                maxFileSize={5242880}
                                label={imagePreviewTitle}
                                withPreview={true}

                            />
                            <TextField

                                error={error}
                                label="Titel"
                                fullWidth
                                margin="normal"
                                variant="outlined"
                                value={title}
                                onChange={(e) => setTitle(e.target.value)}
                                helperText={errorTitle}
                            />

                            <TextField
                                label="Author"
                                fullWidth
                                margin="normal"
                                variant="outlined"
                                value={author}
                                onChange={(e) => setAuthor(e.target.value)}
                            />
                            <TextField
                                error={iserror}
                                helperText={errorDescription}
                                label="Beschreibung"
                                margin="normal"
                                fullWidth
                                multiline
                                rows="4"
                                variant="outlined"
                                value={description}
                                onChange={(e) => setDescription(e.target.value)}
                            />
                            <Grid container spacing={9}>
                                <Grid item xs={5}>
                                    {categories ?
                                        <TextField
                                            error={iserror}
                                            select
                                            value={catagory}
                                            margin="normal"
                                            label="Catagories"
                                            helperText="Bitte wählen Sie eine Katagory aus"
                                            fullWidth
                                            variant="outlined"
                                            onChange={(e) => setCatagory(e.target.value)}
                                        >
                                            {categories.map((option: any) => (
                                                <MenuItem key={option.idcatagory} value={option.bzch}>
                                                    {option.bzch}
                                                </MenuItem>
                                            ))}
                                        </TextField>
                                        : null}

                                </Grid>
                                <Grid item xs={7} container spacing={1}>
                                    <Grid item xs={5}>
                                        <TextField
                                            value={plz}
                                            label="Plz"
                                            margin="normal"
                                            placeholder="Ihre Adresse"
                                            variant="outlined"
                                            fullWidth
                                            onChange={(e) => adressChange("adress", e.target.value)}
                                            defaultValue={book.plz}
                                        />
                                    </Grid>
                                    <Grid item xs={9}>
                                        <TextField
                                            error={iserror}
                                            select
                                            value={city}
                                            margin="normal"
                                            label="Stadt"
                                            fullWidth
                                            variant="outlined"
                                            onChange={(e) => adressChange("city", e.target.value)}
                                        >
                                            {cities.map((option: any) => (
                                                <MenuItem key={option.idCity} value={option.bzch}>
                                                    {option.bzch}
                                                </MenuItem>
                                            ))}
                                        </TextField>
                                    </Grid>
                                </Grid>

                            </Grid>
                        </form>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleClose}>
                            Cancel
                        </Button>
                        <Button color="primary" disabled={editDisabeld} onClick={handleUpload}>
                            Änderung sichern
                        </Button>
                    </DialogActions>
                </div>
            </Dialog>
        </div>
    );
}

export default withRouter(BookEdit)