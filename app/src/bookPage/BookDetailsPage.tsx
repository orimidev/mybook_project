import Container from '@material-ui/core/Container';
import * as React from "react";
import { withRouter } from "react-router-dom";
import { BookContext, BookProvider } from '../contexts/BookContext';
import BookDetails from './BookDetails';
import Suggestion from './Suggestion';

const BookDetailsPage = (props: any) => {
  const { books }: any = React.useContext(BookContext);
  const bookId = props.match.params.id;
  const firstName = props.location.state.firstName;
  const userId = props.location.state.userId;
  return (
    <div>
      <Container maxWidth="lg">
        {books.map((d: any) => {
          const base64Image = 'data:image/jpg;base64,' + d.img;
          return d.bookid === +bookId ? <BookDetails key={bookId} img={base64Image} book={d}/> : null
        }
        )}
        <BookProvider>
          <Suggestion key={bookId} userId={userId} bookId={bookId} firstName={firstName}/>
        </BookProvider>
      </Container>
    </div>
  );
};

export default withRouter(BookDetailsPage);
