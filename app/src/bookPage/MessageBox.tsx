import * as React from "react";
import {Button, TextField} from "@material-ui/core";
import {makeStyles} from "@material-ui/styles";
import {MessageContext} from "../contexts/MessageContext";
import Login from "../header/Login";
import {isNullOrUndefined} from "util";

const useStyles = makeStyles({

    btn: {
        float: 'right',
    }
});

const MessageBox = (props: any) => {
    const userName = localStorage.getItem('user');
    const userId = localStorage.getItem('userId');
    const {msgBoxCreate}: any = React.useContext(MessageContext);
    const [value, setValue] = React.useState('');
    const handleClick = () => {
        if (value !== '') {
            msgBoxCreate('buyer:' + value, props.sellerId, userId, props.book);
            setValue('');
        }
    };
    const placeholder = "Schreibe eine freundliche Nachricht an " + props.firstName + " und bekomme mehr Aufmerksamkeit!";
    const classes = useStyles();
    return (
        <div>
            {userId ?
                <div>
                <TextField
                    margin="normal"
                    placeholder={placeholder}
                    fullWidth
                    multiline
                    variant="outlined"
                    rows="4"
                    value={value}
                    onChange={(e) => setValue(e.target.value)}
                />
                <Button className={classes.btn} color="primary"  onClick={handleClick}>Anfrage
                senden</Button>
                </div>: <Login label="Anfragen senden"/>}

        </div>
    );
};

export default MessageBox;
