import * as React from "react";


import {createMuiTheme, MuiThemeProvider} from "@material-ui/core/styles";
import {ThemeOptions} from "@material-ui/core/styles/createMuiTheme";


const baseThemeOptions: ThemeOptions = {
    overrides: {
        MuiContainer: {
            root: {
                marginBottom: '5vh',
            }
        },
        MuiCard: {
            root: {
                backgroundColor: "#f9fbfb",
                marginTop: "20px",

            }
        },
        MuiButton: {
            root: {
                fontSize: "16px",
                lineHeight: "16px",
                letterSpacing: "0.75px",
                fontWeight: 700,
                textTransform: "uppercase",
            },
            textPrimary: {
                color: "#314152",
                backgroundColor: "#FDCE4B",
                "&:hover": {
                    color: "#314152",
                    backgroundColor: "#FDCE4B"
                }
            },
            textSecondary: {
                color: "#FFFFFF",
                backgroundColor: "#3aafa9",
                "&:hover": {
                    color: "#FFFFFF",
                    backgroundColor: "#47e2dc"
                }
            },
        },
        MuiTableCell: {
            root: {
                padding: 0
            }
        }
    }
};

let themeOptions = baseThemeOptions;
const theme = createMuiTheme(themeOptions);

export function ThemeProvider(props: { children: any }) {
    return <MuiThemeProvider theme={theme}>{props.children}</MuiThemeProvider>;
}
