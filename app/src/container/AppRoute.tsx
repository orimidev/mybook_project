import React from "react";
import {BrowserRouter, BrowserRouter as Router, Route, Switch} from "react-router-dom";
import BookDetailsPage from "../bookPage/BookDetailsPage";
import OfferDeal from "../bookPage/MessageBox";
import BookUpload from "../bookUpload/BookUpload";
import {BookProvider} from '../contexts/BookContext';
import {UserDataProvider} from '../contexts/UserDataContext';
import HeaderContainer from "../header/HeaderContainer";
import Login from "../header/Login";
import Logout from "../header/Logout";
import HomePage from "../homePage/HomePage";
import Messages from "../myData/Messages";
import MyBooks from "../myData/MyBooks";
import MyData from "../myData/MyDataNavigation";
import Profile from "../myData/Profile";
import {MessageProvider} from "../contexts/MessageContext";
import MessageDetail from "../myData/MessageDetail";
import BookEdit from "../bookPage/BookEdit";
import SimpleMap from "../myData/Map";


class AppRoute extends React.Component {
    render() {
        return (<BrowserRouter>
                <MessageProvider>
                    <UserDataProvider>
                        <BookProvider>
                            <Router>
                                <div className="App">
                                    <HeaderContainer/>
                                    <Switch>
                                        <Route path="/my-data" component={MyData}/>
                                        <Route path="/book/:id" component={BookDetailsPage}/>
                                        <Route path="/upload" component={BookUpload}/>
                                        <Route path="/offer-deal" component={OfferDeal}/>
                                        <Route path="/logout" component={Logout}/>
                                        <Route path="/login" component={Login}/>
                                        <Route path="/" exact component={HomePage}/>
                                        <Route path="/messages" component={Messages}/>
                                        <Route path="/mybooks" component={MyBooks}/>
                                        <Route path="/profile" component={Profile}/>
                                        <Route path="/message/:id" component={MessageDetail}/>
                                        <Route path="/editBook/:id" component={BookEdit}/>
                                        <Route path="/map" component={SimpleMap}/>
                                    </Switch>
                                </div>
                            </Router>
                        </BookProvider>
                    </UserDataProvider>
                </MessageProvider>
            </BrowserRouter>

        );


    }

}

export default AppRoute;

