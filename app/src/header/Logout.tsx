import * as React from "react";
import { Redirect } from "react-router-dom";
import { UserDataContext } from "../contexts/UserDataContext";
export default function Logout(props: any) {

  const { removeUserId }: any = React.useContext(UserDataContext);
  localStorage.removeItem('userId');
  localStorage.removeItem('userName');
  removeUserId();

  return (
    <Redirect to='/'/>
  );
}
