import {makeStyles, Typography} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import TextField from "@material-ui/core/TextField";
import * as React from "react";
import {withRouter} from "react-router-dom";
import {UserDataContext} from "../contexts/UserDataContext";
import Registration from "./Registation";
import {MessageContext} from "../contexts/MessageContext";

const useStyles = makeStyles({
    btn: {
        color: "#3aafa9",
        borderColor: "#3aafa9",
        fontSize: 13,
        fontWeight: 600,
    },
    btnLink: {
        color: "red",
        fontSize: 13,
        fontWeight: 600,
    },
    pLink: {
        textDecoration: "underline",
        "&:hover": {
            color: "#9e9e9e",
            cursor: "pointer"
        }
    },
});

function Login(props: any) {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);
    const [errorUserName, setErrorUserName] = React.useState();
    const [errorPassord, setErrorPassword] = React.useState();
    const [errorLogin, setErrorLogin] = React.useState();
    const [username, setUsername] = React.useState();
    const [password, setPassword] = React.useState();
    const {login, error, setError}: any = React.useContext(UserDataContext);
    const {getAllmsg, setLoading}: any = React.useContext(MessageContext);

    const handleClose = () => {
        setOpen(false);
        handleReset()
    };
    const handleReset = () => {
        setUsername('');
        setPassword('');
        setErrorLogin('');
        setErrorUserName('');
        setErrorPassword('');
        setError(false)
    };
    const handleClickOpen = () => {
        setOpen(true);
    };
    const goToBookUpload = () => {
        props.history.push("/upload");
    };

    const handleLogin = async () => {
            const isLogin = await login(username, password, props.label);

            await handleReset();
            if (props.label === "Verschenken" && isLogin) {
               await goToBookUpload()
            }
            await setLoading(true);
            await getAllmsg();
            return
    };

    return (
        <div>
            {props.header ? <Button variant={props.label === "Verschenken"? "text": "outlined"}
                                    style={props.label === "Verschenken"? {backgroundColor: "#FDCE4B", marginRight: "1vw",
                                    color: "#314152"}: {display: "block"}}
                                    className={classes.btn} onClick={handleClickOpen}>
                {props.label}
            </Button> : props.textOnly ? <p className={classes.pLink} onClick={handleClickOpen}>einloggen</p> :
                <Button style={{ display: "none",backgroundColor: "#FDCE4B"}} onClick={handleClickOpen}>
                    {props.label}
                </Button>}


            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="form-dialog-title"
            >
                <DialogTitle id="form-dialog-title">Einloggen</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Bitte melden Sie sich an, um alle Funktionen nutzen zu können, oder
                        <Registration/>
                    </DialogContentText>
                    <TextField
                        id="outlined-with-placeholder"
                        autoFocus
                        margin="normal"
                        label="User Name"
                        type="text"
                        variant="outlined"
                        onChange={(e) => setUsername(e.target.value)}
                        fullWidth
                    />
                    <TextField
                        id="outlined-password-input"
                        label="Password"
                        type="password"
                        margin="normal"
                        variant="outlined"
                        onChange={(e) => setPassword(e.target.value)}
                        fullWidth
                        onKeyPress={event => {
                            if (event.key === 'Enter') {
                                handleLogin()
                            }
                        }}

                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose}>
                        Cancel
                    </Button>
                    <Button onClick={handleLogin} color="primary">
                        Einloggen
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}

export default withRouter(Login)