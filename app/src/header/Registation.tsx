import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import TextField from "@material-ui/core/TextField";
import * as React from "react";
import {UserDataContext} from "../contexts/UserDataContext";
import {makeStyles} from "@material-ui/styles";
import Login from "./Login";

const useStyles = makeStyles({
    btn: {
        color: "#3aafa9",
        fontSize: 13,
        fontWeight: 600,
        borderColor: "#3aafa9"
    },
    pLink: {
        textDecoration: "underline",
        "&:hover": {
            color: "#9e9e9e",
            cursor: "pointer"
        }
    },
    flexLink: {
        display: "flex"
    }
});
export default function Registration(props: any) {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);
    const [username, setUsername] = React.useState('');
    const [password, setPassword] = React.useState('');
    const [strasse, setStrasse] = React.useState('');
    const [plz, setPlz] = React.useState('');
    const [firstName, setFirstName] = React.useState('');
    const [lastName, setLastName] = React.useState('');
    const {userRegistration}: any = React.useContext(UserDataContext);
    const handleClose = () => {
        setOpen(false);
        setUsername('');
        setPassword('');
    };
    const handleClickOpen = () => {
        setOpen(true);
    };
    const handleRegistration = () => {
            userRegistration(plz, strasse, firstName, lastName, password, username);
            handleClose();
    };

    return (
        <div>
            {props.header ?
                <Button className={classes.btn} variant="outlined" onClick={handleClickOpen}>
                    Registrieren
                </Button>
                :
                <DialogContentText onClick={handleClickOpen}>
                    <div className={classes.flexLink}><p className={classes.pLink}>registrieren</p> <p>  &nbsp; Sie
                        sich.</p></div>
                </DialogContentText>

            }

            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="form-dialog-title"
            >
                <DialogTitle id="form-dialog-title">Registration</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                       <div className={classes.flexLink }><p>Sie haben bereits einen Account? Wenn ja dann bitte sich hier &nbsp;</p>  <Login label="Login" textOnly={true}/></div>
                    </DialogContentText>
                    <TextField
                        autoFocus
                        margin="normal"
                        label="User Name"
                        type="text"
                        name="userName"
                        variant="outlined"
                        onChange={(e) => setUsername(e.target.value)}
                        fullWidth
                        required
                    />
                    <TextField
                        margin="normal"
                        label="Vorname"
                        type="text"
                        name="firstName"
                        variant="outlined"
                        onChange={(e) => setFirstName(e.target.value)}
                        fullWidth
                        required
                    />
                    <TextField
                        margin="normal"
                        label="Nachname"
                        type="text"
                        name="lastName"
                        variant="outlined"
                        onChange={(e) => setLastName(e.target.value)}
                        fullWidth
                        required
                    />
                    <TextField
                        label="Password"
                        type="password"
                        autoComplete="current-password"
                        margin="normal"
                        variant="outlined"
                        name="password"
                        onChange={(e) => setPassword(e.target.value)}
                        fullWidth
                        required
                    />

                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="secondary">
                        Cancel
                    </Button>
                    <Button onClick={handleRegistration} color="primary">
                        Registration
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}

