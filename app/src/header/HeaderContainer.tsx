import {Badge, Button, Container, IconButton, Link, makeStyles, Menu, MenuItem} from "@material-ui/core";
import AppBar from "@material-ui/core/AppBar";
import Tab from "@material-ui/core/Tab";
import Toolbar from "@material-ui/core/Toolbar";
import AccountCircle from '@material-ui/icons/AccountCircle';
import HomeIcon from '@material-ui/icons/Home';
import * as React from "react";
import {withRouter} from "react-router-dom";
import {isNullOrUndefined} from "util";
import Snackbars from "../components/SnackBar";
import {CategoryProvider} from "../contexts/CategoryContext";
import {UserDataContext} from '../contexts/UserDataContext';
import Category from "./Category";
import Login from "./Login";
import Registration from "./Registation";
import SearchAppBar from "./Search";
import {MessageContext} from "../contexts/MessageContext";
import MailIcon from '@material-ui/icons/Mail';
import MapIcon from '@material-ui/icons/Map';


const useStyles = makeStyles({
    root: {
        background: "#ffffff",
        color: "#69839c",
        marginBottom: 0,
        paddingTop: "1vh"
    },
    link: {
        textDecoration: "none",
        color: "#69839c",
        fontSize: 13,
        fontWeight: 700,
    },
    verschenken: {
        color: "#69839c"
    },
    btnHover:{
        '&:hover': {
            backgroundColor: "#d4d4d4"
        },
    },
    iconHover: {
        color: "#3aafa9",
        '&:hover': {
            backgroundColor: "#d4d4d4"
        },
        borderRadius: "50%"
    },
});
const HeaderContainer = (props: any) => {
    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
    const {activMsg, loading}: any = React.useContext(MessageContext);
    const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const classes = useStyles();
    const {firstName, userId}: any = React.useContext(UserDataContext);
    if (isNullOrUndefined(userId)) {
        return (
            <CategoryProvider>
            <div>
                <Container maxWidth="lg">
                    <AppBar className={classes.root} position="static">
                        <Toolbar>
                            <Button   className={classes.btnHover}>
                            <Tab
                            textColor="white"
                            icon={<HomeIcon
                            className={classes.iconHover}
                            fontSize="large"
                        />} href={'/'}/>
                            </Button>

                            <SearchAppBar props={props}/>
                            <Login header={true} label="Verschenken"/>
                            <Login header={true} label="Einloggen"/>
                            <Button>
                                <Registration header={true}/>
                            </Button>
                        </Toolbar>
                            <Category/>
                    </AppBar>
                </Container>
            </div>
            </CategoryProvider>
        );
    } else {
        return (
            <CategoryProvider>
            <div>
                <Container maxWidth="lg">
                    {!isNullOrUndefined(firstName) ? <Snackbars vorName={firstName}/> : null}
                    <AppBar className={classes.root} position="static">
                        <Toolbar>
                            <Button   className={classes.btnHover}>
                                <Tab
                                    textColor="white"
                                    icon={<HomeIcon
                                        className={classes.iconHover}
                                        fontSize="large"
                                    />} href={'/'}/>
                            </Button>

                            <SearchAppBar props={props}/>
                            <Button className={classes.btnHover}>
                                <Tab
                                    textColor="white"
                                    icon={<MapIcon
                                        className={classes.iconHover}
                                        fontSize="large"
                                    />} href={'/map'}/>
                            </Button>
                            <Button>
                                <Tab label="Verschenken"
                                     style={{borderRadius: "8px", borderStyle: "solid", borderWidth: "thin", borderColor: "#FDCE4B",
                                     backgroundColor: "#FDCE4B",  color: "#314152" ,fontWeight: 600}}
                                     textColor="white" className={classes.verschenken}
                                     href={'/upload'}/>
                            </Button>
                            <IconButton href="/messages" aria-label="show 17 new notifications" className={classes.iconHover}>
                                {loading === false ? <Badge badgeContent={activMsg.length} color="secondary">
                                    <MailIcon/>
                                </Badge> : null}
                            </IconButton>
                            <div>
                                <IconButton
                                    className={classes.iconHover}
                                    aria-label="account of current user"
                                    aria-controls="menu-appbar"
                                    aria-haspopup="true"
                                    onClick={handleClick}
                                >
                                    <AccountCircle/>
                                </IconButton>
                                <Menu
                                    id="simple-menu"
                                    anchorEl={anchorEl}
                                    keepMounted
                                    open={Boolean(anchorEl)}
                                    onClose={handleClose}
                                    getContentAnchorEl={null}
                                    anchorOrigin={{
                                        vertical: 'bottom',
                                        horizontal: 'center',
                                    }}
                                    transformOrigin={{
                                        vertical: 'top',
                                        horizontal: 'center',
                                    }}
                                >
                                    <Link className={classes.link} href="/mybooks"><MenuItem>Meine Bücher</MenuItem>
                                    </Link>
                                    <Link className={classes.link} href="/messages"><MenuItem>Meine Nachricht</MenuItem>
                                    </Link>
                                    <Link className={classes.link} href="/profile"><MenuItem>Profile </MenuItem> </Link>
                                    <Link className={classes.link} href="/Logout"><MenuItem>Logout</MenuItem> </Link>
                                </Menu>
                            </div>
                        </Toolbar>

                            <Category></Category>

                    </AppBar>
                </Container>
            </div>
    </CategoryProvider>
        )

    }

};

export default withRouter(HeaderContainer);
