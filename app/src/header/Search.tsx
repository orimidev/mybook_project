import React from "react";
import InputBase from "@material-ui/core/InputBase";
import SearchIcon from "@material-ui/icons/Search";
import {createStyles, fade, makeStyles, Theme} from "@material-ui/core/styles";
import {BookContext} from "../contexts/BookContext";
import {CategoryContext} from "../contexts/CategoryContext";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1
        },

        search: {
            position: 'relative',
            borderRadius: theme.shape.borderRadius,
            backgroundColor: fade("#3aafa9", 0.15),
            '&:hover': {
                backgroundColor: fade("#3aafa9", 0.25),
            },
            marginRight: theme.spacing(2),
            marginLeft: 0,
            width: '100%',
            [theme.breakpoints.up('sm')]: {
                marginLeft: theme.spacing(3),
                width: 'auto',
            },
        },
        searchIcon: {
            width: theme.spacing(7),
            height: '100%',
            position: 'absolute',
            pointerEvents: 'none',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            color: "#3aafa9"
        },
        inputRoot: {
            color: "inherit"
        },
        inputInput: {
            padding: theme.spacing(1, 1, 1, 7),
            transition: theme.transitions.create('width'),
            width: '100%',
            [theme.breakpoints.up('sm')]: {
                width: 120,
                '&:focus': {
                    width: 200,
                },
            },
        },
    })
);

export default function SearchAppBar(props: any) {
    const classes = useStyles();
    const {filterAndReloadBooks}: any = React.useContext(BookContext);
    const { reload }: any = React.useContext(CategoryContext);

    const searchChangeHandle = (e: any) => {
        reload();
        props.props.history.push('/');
        filterAndReloadBooks(e.target.value);
    };
    return (
        <div className={classes.root}>
            <div className={classes.search}>
                <div className={classes.searchIcon}>
                    <SearchIcon/>
                </div>
                <div className={classes.searchIcon}>
                    <SearchIcon/>
                </div>
                <InputBase
                    placeholder="Suche..."
                    classes={{
                        root: classes.inputRoot,
                        input: classes.inputInput
                    }}
                    inputProps={{"aria-label": "search"}}
                    onChange={searchChangeHandle}
                />
            </div>
        </div>
    );
}
