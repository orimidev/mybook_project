import { Button, ButtonGroup } from "@material-ui/core";
import * as React from "react";
import { isNullOrUndefined } from 'util';
import { CategoryContext } from '../contexts/CategoryContext';
import { BookContext } from "../contexts/BookContext";
import { makeStyles } from "@material-ui/styles";
import { withRouter } from "react-router-dom";
const useStyles = makeStyles({
  btn: {
    color: "#feffff",
    fontSize: 13,
    fontWeight: 600,
    marginTop: "1vh",
    paddingTop: "1vh",
    paddingBottom: "1vh",
    '&:hover': {
      backgroundColor: "#41dad4"
    }
  },
  btnChoosed: {
    color: "#feffff",
    backgroundColor: "#3dd7d1",
    fontSize: 15,
    fontWeight: 600,
    marginTop: "1vh",
    paddingTop: "1vh",
    paddingBottom: "1vh",
    '&:hover': {
      backgroundColor: "#41dad4"
    }
  }

  , root: {
    textAlign: "center",
    background: "#3aafa9"

  }
});
const Category = (props: any) => {
  const classes = useStyles();
  const { categories,  selectedKategory, setSelectedKategory}: any = React.useContext(CategoryContext);
  const { filterBookFromCatagory }: any = React.useContext(BookContext);

  const filterBook = (catagoryId?: number) => {
    props.history.push("/");
    setSelectedKategory(catagoryId);
    filterBookFromCatagory(catagoryId);
  };

  let load = true;
  if (isNullOrUndefined(categories)) {
    load = true
  } else (load = false);
  return (
    <div className={classes.root} >
      {!load ? categories.map((d: any) => (
        <Button
          className={selectedKategory === d.idcatagory
              ? classes.btnChoosed:classes.btn}
          key={d.idcatagory}
          onClick={() => filterBook(d.idcatagory)} >{d.bzch}
        </Button>
      )) : null}
    </div>
  );
};

export default withRouter(Category);
