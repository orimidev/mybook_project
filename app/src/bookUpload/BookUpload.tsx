import {Container, Theme} from "@material-ui/core";
import {createStyles, makeStyles} from "@material-ui/styles";
import * as React from "react";
import {isNullOrUndefined} from "util";
import {CategoryProvider} from '../contexts/CategoryContext';
import {UserDataContext} from "../contexts/UserDataContext";
import BookForm from "./BookForm";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            margin: 40
        },
        text: {
            fontWeight: 300,
            display: "inline-block"
        },
        textDiv: {
            textAlign: "center",
        }
    }),

);
const BookUpload = () => {
    const classes = useStyles();
    const { userId }: any = React.useContext(UserDataContext);
    let loggedIn = true;
    if (isNullOrUndefined(userId)) {
        loggedIn = false;
    }
    return (
        <Container maxWidth="md">
                <CategoryProvider>
                    <h1>Buch verschenken</h1>
                    <BookForm/>
                </CategoryProvider>
        </Container>
    )

};

export default BookUpload;
