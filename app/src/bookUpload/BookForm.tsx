import {Button, Grid, MenuItem, TextField} from "@material-ui/core";
import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';
import * as React from "react";
import ImageUploader from 'react-images-upload';
import {isNullOrUndefined} from "util";
import {BookContext} from "../contexts/BookContext";
import {CategoryContext} from '../contexts/CategoryContext';
import {withRouter} from "react-router-dom";
import ajax from "superagent";



const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            margin: 40
        },
        container: {
            display: 'flex',
            flexWrap: 'wrap',
        },
        textField: {
            marginLeft: theme.spacing(1),
            marginRight: theme.spacing(1),
        },
        dense: {
            marginTop: theme.spacing(2),
        },
        menu: {
            width: 200,
        },
        button: {
            margin: theme.spacing(1),
            float: 'right'
        },
        input: {
            display: 'none',
        },
    }),
);

const BookForm = (props: any) => {
    const [state, setState] = React.useState<{ title: string; author: string, description: string, catagory: any, img: any }>({
        title: '', author: '', description: '', catagory: '', img: ''
    });
    const {addBook, cities}: any = React.useContext(BookContext);
    const {categories}: any = React.useContext(CategoryContext);
    const [title, setTitle] = React.useState();
    const [lat, setLat] = React.useState();
    const [lng, setLng] = React.useState();
    const [author, setAuthor] = React.useState();
    const [description, setDescription] = React.useState();
    const [catagory, setCatagory] = React.useState();
    const [city, setCity] = React.useState();
    const [plz, setPlz] = React.useState();
    const [img, setImg] = React.useState();
    const [errorTitle, setErrorTitle] = React.useState();
    const [errorDescription, setErrorDescripton] = React.useState();
    const [error, setError] = React.useState(false);
    const [iserror, setisError] = React.useState(false);

    const [imagePreviewTitle, setImagePreviewTitle] = React.useState("Maximale Größe: 5mb, Format: jpg, gif, png, jpeg");
    const classes = useStyles();

    const handleUpload = (e: any) => {
        const userId = localStorage.getItem('userId');
        e.preventDefault();
        const isValid = validate();
        if (isValid) {
            ajax.get(" https://maps.googleapis.com/maps/api/geocode/json?address=" + plz + "," +  city.bzch + ",Germany&key=AIzaSyDsHlrJFZ-0D5w16OY7obr7VfMBMWXX4Ro")
                .end((err: any, res: any) => {
                    const {lat, lng} = res.body.results[0].geometry.location;
                    setLat(lat);
                    setLng(lng);
                    addBook(title, author, description, catagory, img, userId, plz, city, lat.toString(), lng.toString());
                });

            setErrorDescripton('');
            setErrorTitle('');
            setTitle('');
            setAuthor('');
            setDescription('');
            setError(false);
            setisError(false);
            props.history.push('/');
        }
    };


    const validate = () => {
        let emailError = "";
        let descriptionError = "";
        if (!title) {
            emailError = "Titel darf nicht lerr sein";
            setError(true);
            setErrorTitle(emailError);
            return false
        }
        if (!description) {
            descriptionError = "Bitte fügen Sie eine Beschreibung hinzu.";
            setisError(true);
            setErrorDescripton(descriptionError);
            return false
        }
        return true
    };


    const onChangeImg = (file: any) => {
        getBase64(file[0], (result: any) => {
            setImagePreviewTitle(file[0].name);
            setImg(result.replace('data:image/png;base64,', '')
                .replace('data:image/jpeg;base64,', '')
                .replace('data:image/jpg;base64,', '')
                .replace('data:image/gif;base64,', ''));
        });
    };
    const getBase64 = (file: any, cb: any) => {
        if (!isNullOrUndefined(file)) {
            let reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function () {
                cb(reader.result)
            };
            reader.onerror = function (error) {
                console.log('Error: ', error);
            };
        } else return null;

    };

    return (
        <div>
            <form>
                <ImageUploader
                    withIcon={true}
                    buttonText='Photo hinzufügen'
                    onChange={onChangeImg}
                    imgExtension={['.jpg', '.jpeg', '.gif', '.png']}
                    maxFileSize={5242880}
                    label={imagePreviewTitle}
                    withPreview={true}
                />
                <TextField

                    error={error}
                    label="Titel"
                    placeholder="Geben Sie einen Bookstitel ein"
                    fullWidth
                    margin="normal"
                    variant="outlined"
                    value={title}
                    onChange={(e) => setTitle(e.target.value)}
                    helperText={errorTitle}
                />

                <TextField
                    label="Author"
                    placeholder="Geben Sie den Name des Autor ein"
                    fullWidth
                    margin="normal"
                    variant="outlined"
                    value={author}
                    onChange={(e) => setAuthor(e.target.value)}
                />
                <TextField
                    error={iserror}
                    helperText={errorDescription}
                    label="Beschreibung"
                    margin="normal"
                    placeholder="Beschreiben Sie Ihr Book..."
                    fullWidth
                    multiline
                    rows="4"
                    variant="outlined"
                    value={description}
                    onChange={(e) => setDescription(e.target.value)}
                />
                <Grid container spacing={9}>
                    <Grid item xs={5}>
                        <TextField
                            error={iserror}
                            select
                            value={catagory}
                            margin="normal"
                            label="Catagories"
                            helperText="Bitte wählen Sie eine Katagory aus"
                            fullWidth
                            variant="outlined"
                            onChange={(e) => setCatagory(e.target.value)}
                        >
                            {categories.map((option: any) => (
                                <MenuItem key={option.idcatagory} value={option}>
                                    {option.bzch}
                                </MenuItem>
                            ))}
                        </TextField>
                    </Grid>
                    <Grid item xs={7} container spacing={1}>
                        <Grid item xs={3}>
                            <TextField
                                value={plz}
                                label="Plz"
                                margin="normal"
                                placeholder="Ihre Plz"
                                variant="outlined"
                                fullWidth
                                onChange={(e) => setPlz(e.target.value)}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />
                        </Grid>
                        <Grid item xs={9}>
                            <TextField
                                error={iserror}
                                select
                                value={city}
                                margin="normal"
                                label="Stadt"
                                helperText="Bitte geben Sie Ihre Addresse ein"
                                fullWidth
                                variant="outlined"
                                onChange={(e) => setCity(e.target.value)}
                            >
                                {cities.map((option: any) => (
                                    <MenuItem key={option.idCity} value={option}>
                                        {option.bzch}
                                    </MenuItem>
                                ))}
                            </TextField>

                        </Grid>
                    </Grid>

                </Grid>

                <Button color="primary" className={classes.button}  onClick={handleUpload}>Speichern</Button>
            </form>
        </div>

    );
};

export default withRouter(BookForm)
