import * as React from "react";
import ajax from 'superagent';
import {isNullOrUndefined} from "util";


export const MessageContext = React.createContext({});
export const MessageProvider = (props: any) => {
    const url = 'http://localhost:8080/msg_box/';
    const userUrl = 'http://localhost:8080/user/';
    const [loading, setLoading] = React.useState(true);
    const [loadingById, setLoadingById] = React.useState(true);
    const [msg, setMsg] = React.useState();
    const [msgBoxById, setMsgBoxById] = React.useState();
    const [activMsg, setActivMsg] = React.useState();
    const [bookExchangeMsg, setBookExchangeMsg] = React.useState();
    const superagent = require('superagent');
    const userId = localStorage.getItem('userId');


    const getMsgBoxById = (msgBoxId: number) => {
        if (loadingById) {
            ajax.get(url + msgBoxId)
                .end((err: any, res: any) => {
                    setMsgBoxById(res.body);
                    setLoadingById(false);
                });
        }
    };

    const getAllmsg = () => {
        if (loading && !isNullOrUndefined(userId)) {
            ajax.get(url + '/user/' + userId)
                .end((err: any, res: any) => {
                    const data = res.body.filter((msg: any) => {
                        return msg.newmsgForSeller == 1 && msg.seller.userId == userId ||
                            msg.newmsgForBuyer == 1 && msg.buyer.userId == userId
                    });
                    setMsg(res.body);
                    setActivMsg(data);
                    setLoading(false);
                    localStorage.setItem('msgLength', res.body.length);
                });
        }
    };

    const chat = (newMessage: string, messageBox: any, isBuyer: boolean) => {
        let newMsg = messageBox;
        const textToSplit = "XUONGDONGNHE";
        if (isBuyer) {
            newMsg.newmsgForSeller = 1;
            newMsg.msg = newMsg.msg + textToSplit + 'buyer:' + newMessage
        } else {
            newMsg.newmsgForBuyer = 1;
            newMsg.msg = newMsg.msg + textToSplit + 'seller:' + newMessage
        }
        superagent
            .post(url + 'update')
            .send(newMsg)
            .set('accept', 'application/json')
            .end((err: any, res: any) => {
            });
        getMsgBoxById(newMsg.msgBoxId);
    };

    const readMsg = (messageBox: any, isBuyer: boolean) => {
        const newMsg = messageBox;
        if (isBuyer) {
            newMsg.newmsgForBuyer = 0
        } else {
            newMsg.newmsgForSeller = 0
        }
        superagent
            .post(url + 'update')
            .send(newMsg)
            .set('accept', 'application/json')
            .end((err: any, res: any) => {
                setLoading(true);
                getAllmsg();
            });
    };


    const setMultiBookExchangeText = (bookNames: any) => {
        let bookExchange: any[] = [];
        if (bookExchangeMsg) {
            bookExchangeMsg.map((d: any) => {
                bookExchange.push(d);
            })
        }
        if (bookExchange.length === 0) {
            bookExchange.push(bookNames);
        } else {
            if(!bookExchange.includes(bookNames)) {
                bookExchange.push(bookNames);
            }
        }

        setBookExchangeMsg(bookExchange);
    };

    const updateBookExchangeMsg = (books: any[]) => {
        setBookExchangeMsg(books);
    };


    const msgBoxCreate = (msg: string, sellerId: number, buyerId: number, book: any) => {
        fetch(userUrl + sellerId)
            .then(res => res.json()).then(seller => {
            fetch(userUrl + buyerId)
                .then(res => res.json()).then(buyer => {
                const msgBox = {msg, seller, buyer, book};
                const finalMsgBoxToSend = msgAddOwner(msgBox, true, false);
                superagent
                    .post(url + '/update')
                    .send(finalMsgBoxToSend)
                    .set('accept', 'application/json')
                    .end((err: any, res: any) => {
                    });
            })
        });
    };

    const msgAddOwner = (msgBox: any, newMsgForSeller: boolean, newMsgForBuyer: boolean) => {
        const out = msgBox;
        if (newMsgForSeller) {
            out.newmsgForSeller = 1;
            out.newmsgForBuyer = 0
        } else if (newMsgForBuyer) {
            out.newmsgForBuyer = 1;
            out.newmsgForSeller = 0;
        }
        return out;
    };

    const removeMsg = (MsgBoxId: number) => {
        ajax.get(url + 'remove/' + MsgBoxId)
            .end((err: any, res: any) => {
                setLoading(true);
                getAllmsg();
            });
    };
    getAllmsg();
    return (
        <div>
            <MessageContext.Provider
                value={{
                    msg,
                    readMsg,
                    msgBoxCreate,
                    updateBookExchangeMsg,
                    setMultiBookExchangeText,
                    bookExchangeMsg,
                    loading,
                    setLoading,
                    getAllmsg,
                    chat,
                    removeMsg,
                    activMsg,
                    getMsgBoxById,
                    msgBoxById
                }}>
                {props.children}
            </MessageContext.Provider>


        </div>

    );
};
