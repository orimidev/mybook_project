import * as React from "react";
import {MessageContext} from "./MessageContext";

export const UserDataContext = React.createContext({});
export const UserCheck = React.createContext({});


export const UserDataProvider = (props: any) => {
    const [username, setUsername] = React.useState();
    const [user, setUser] = React.useState();
    const [firstName, setFirstName] = React.useState();
    const [password, setPassword] = React.useState();
    const [error, setError] = React.useState(false);
    const [isLogin, setIsLogin] = React.useState(false);
    const [userId, setUserId] = React.useState(localStorage.getItem('userId'));
    const url = 'http://localhost:8080/user';
    const superagent = require('superagent');

    const removeUserId = () => {
        setUserId(null);
    };

    const updateUser = async (user: any, address: any) => {
        await superagent
            .post(url + '/changeAdress')
            .send(address)
            .set('accept', 'application/json')
            .end((err: any, res: any) => {
                superagent
                    .post(url + '/register')
                    .send(user)
                    .set('accept', 'application/json')
                    .end((err: any, res: any) => {
                        setUser(res.body);
                    });
            });
    };

    const findUserById = async (userId: string) => {
        await fetch(url + '/' + userId)
            .then(res => res.json()).then(u => {
                setUser(u);
            });
    };

    const login = async (userName: string, password: string, label?: string) => {
        await fetch(url + '/user?usename=' + userName + '&password=' + password)
            .then(res => res.json()).then(user => {
                if (user.firstName !== "unknown") {
                    setFirstName(user.firstName);
                    setUserId(user.userId);
                    setIsLogin(true);
                    setUser(user);
                    localStorage.setItem('userId', user.userId);
                    localStorage.setItem('user', user.firstName);
                } else {
                   alert("wrong password or usename");
                }
            });
        return isLogin;
    };
    const userRegistration = (plz: number, strasse: string, firstName: string, lastName: string, password: string, userName: string) => {
        const user = {
            address: {
                city: {
                    idCity: 1,
                    bzch: "Berlin"
                },
                plz,
                strasse
            },
            firstName,
            lastName,
            password,
            userName
        };
        superagent
            .post(url + '/register')
            .send(user)
            .set('accept', 'application/json')
            .end((err: any, res: any) => {
            });
    };


    return (
        <UserDataContext.Provider value={{
            removeUserId,
            username,
            user,
            updateUser,
            password,
            firstName,
            userRegistration,
            findUserById,
            login,
            userId,
            error,
            setError,
            isLogin
        }}>
            {props.children}
        </UserDataContext.Provider>
    )
};