import * as React from "react";
import ajax from 'superagent';


export const CategoryContext = React.createContext({

});


export const CategoryProvider = (props: any) => {

  const [categories, setCategories] = React.useState();
  const [selectedKategory, setSelectedKategory] = React.useState();
  const [load, setLoaded] = React.useState(true);
  const url = "http://localhost:8080/kata/list";
  if (load) {
    ajax.get(url)
      .end((err: any, res: any) => {
        setCategories(res.body);
        setLoaded(false);
      });
  }

  const reload = () => {
    setLoaded(false);
    setSelectedKategory(undefined);
  };
  return (
    <div>
      {!load ? <CategoryContext.Provider value={{ categories , reload, selectedKategory, setSelectedKategory}}>
        {props.children}
      </CategoryContext.Provider> : null
      }

    </div >

  );
};
