import * as React from "react";
import ajax from 'superagent';
import {isNullOrUndefined} from "util";

export const BookContext = React.createContext({});

export const BookProvider = (props: any) => {
    const url = "http://localhost:8080/books/";
    const userUrl = 'http://localhost:8080/user';
    const [books, setBooks] = React.useState();
    const [bookId, setBookId] = React.useState();
    const [allBooks, setAllBooks] = React.useState();
    const [userBooks, setUserBooks] = React.useState();
    const [myBooks, setMyBooks] = React.useState();
    const [cities, setCities] = React.useState();
    const [loading, setLoading] = React.useState(true);
    const [userloading, setUserLoading] = React.useState(true);
    const [myBookloading, setMyBookLoading] = React.useState(true);
    const [cityLoading, setCityLoading] = React.useState(true);
    const [userId, setUserId] = React.useState(localStorage.getItem('userId'));
    const superagent = require('superagent');


    const getAllBooks = () => {
        ajax.get(url + 'list')
            .end((err: any, res: any) => {
                setBooks(res.body);
                setAllBooks(res.body);
                setLoading(false);
            });
    };

    if (loading) {
        getAllBooks();
    }


    const getMyBooks = (userId: number) => {
        if (myBookloading) {
            ajax.get(url + 'user/' + userId)
                .end((err: any, res: any) => {
                    setMyBooks(res.body);
                    setMyBookLoading(false);
                    setLoading(false);
                });
        }
    };


    /**
     * Soll alle Bücher eines Users aufrufen, außer das, was gerade ausgewählt ist.
     * @param userId : die Person, der das Book gehört
     * @param newBookId : das gerade ausgewählte Book
     */
    const getUserBooks = (userId: number, newBookId: number) => {
        if (userloading || newBookId !== bookId) {
            ajax.get(url + 'user/' + userId)
                .end((err: any, res: any) => {
                    setUserBooks(res.body.filter((book: any) => book.bookid != newBookId));
                    setBookId(newBookId);
                    setUserLoading(false)
                })

        }
    };


    const filterAndReloadBooks = (value: string) => {
        if (isNullOrUndefined(value) || value === '') {
            getAllBooks();
        }
        ajax.get(url + 'filter?search=' + value)
            .end((err: any, res: any) => {
                setBooks(res.body);
                setAllBooks(res.body);
                setLoading(false);
            });

    };


    /**
     * soll eine List von Büchern aufrufen, welche dieser Katagory gehört
     * @param id : CatagoryId, die gerade ausgewählt ist
     */
    const filterBookFromCatagory = (id: number) => {
        setBooks(allBooks.filter((book: any) => book.katagory.idcatagory == id));
    };



    const removeBook = (bookId: number, userId: number) => {
        ajax.get(url + 'remove/' + bookId)
            .end((err: any, res: any) => {
                getAllBooks();
            });
    };
    /**
     * Create new Book
     * @param bzch
     * @param author
     * @param deskription
     * @param katagory
     * @param img
     * @param userId
     * @param plz
     * @param city
     * @param lat
     * @param lng
     */
    const addBook = (bzch: string, author: string, deskription: string,
                     katagory: any, img: any, userId: number, plz: number,
                     city: any, lat: any, lng: any) => {
        fetch(userUrl + '/' + userId)
            .then(res => res.json()).then(user => {
            const book = {bzch, author, deskription, katagory, aktiv: true, img, userOfBook: user, plz, city, lat, lng};
            superagent
                .post(url + '/register')
                .send(book)
                .set('accept', 'application/json')
                .end((err: any, res: any) => {
                    setLoading(true);
                    getAllBooks();
                });
        });
    };
    const editBook = (book: any) => {
        superagent
            .post(url + '/register')
            .send(book)
            .set('accept', 'application/json')
            .end((err: any, res: any) => {
                setLoading(true);
                getAllBooks();
            });

    };


    if (cityLoading) {
        ajax.get(url + 'city')
            .end((err: any, res: any) => {
                setCities(res.body);
                setCityLoading(false);
            });
    }


    return (
        <div>
            {!loading ? <BookContext.Provider value={{
                filterAndReloadBooks,
                books,
                removeBook,
                addBook,
                userBooks,
                myBooks,
                getUserBooks,
                myBookloading,
                setMyBookLoading,
                filterBookFromCatagory,
                getMyBooks,
                userloading,
                cities,
                editBook
            }}>
                {props.children}
            </BookContext.Provider> : null}

        </div>

    );
};

