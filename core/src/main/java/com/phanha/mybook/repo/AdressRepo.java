package com.phanha.mybook.repo;

import com.phanha.mybook.model.Adress;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface AdressRepo extends CrudRepository<Adress, Integer> {



    Adress save(Adress adress);


}
