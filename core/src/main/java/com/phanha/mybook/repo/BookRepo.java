package com.phanha.mybook.repo;

import com.phanha.mybook.model.Book;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepo extends CrudRepository<Book, Integer> {

    List<Book> findAll();

    @Query("select b from Book b where b.userOfBook.userId <>:id and b.aktiv = 1")
    List<Book> findAllBooksNotFromThisUser(Integer id);

    @Query("select b from Book b where b.author LIKE CONCAT('%', :search, '%') or b.bzch LIKE CONCAT('%', :search, '%')  ")
    List<Book> findAllBooksByKey(String search);

    @Query("select b from Book b where b.userOfBook.userId =:id and b.aktiv = 1")
    List<Book> findAllBooksFromThisUser(Integer id);

    @Query("select b from Book b where b.byerOfBook.userId =:id")
    List<Book> findAllBookToPickUpFromThisUsers(Integer id);

    Book save(Book book);

    Book findByBookid(Integer bookId);

}
