package com.phanha.mybook.repo;

import com.phanha.mybook.model.Katagory;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface KataRepo extends CrudRepository<Katagory, Integer> {

    List<Katagory> findAll();



}
