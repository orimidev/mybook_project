package com.phanha.mybook.repo;


import com.phanha.mybook.model.Msgbox;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MsgRepo extends CrudRepository<Msgbox, Integer> {

    List<Msgbox> findAll();

    @Query("select m from Msgbox m where m.buyer.userId =:id " +
            "or m.seller.userId =:id order by m.timestamp desc")
    List<Msgbox> findAllMsgForUser(Integer id);


    Msgbox findByMsgBoxId(Integer id);


    @Query("select m from Msgbox m where m.seller.userId =:id and m.newmsgForSeller = 0")
    List<Msgbox> findNewMsgForSeller(Integer id);

    @Query("select m from Msgbox m where m.buyer.userId =:id and m.newmsgForBuyer = 0")
    List<Msgbox> findNewMsgForBuyer(Integer id);

    Msgbox save(Msgbox msgbox);


}
