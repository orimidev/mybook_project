package com.phanha.mybook.repo;

import com.phanha.mybook.model.City;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CityRepo extends CrudRepository<City, Integer> {

    List<City> findAll();



}
