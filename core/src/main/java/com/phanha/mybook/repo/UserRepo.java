package com.phanha.mybook.repo;

import com.phanha.mybook.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepo extends CrudRepository<User, Integer> {

    List<User> findAll();

    User save(User tour);

    User findByUserId(Integer userId);

    @Query("select u from User u where u.userName =:u AND u.password =:p")
    User findUserByUseNameAndPassword(String u, String p);

    @Query("select u from User u where u.userId =:id")
    User findUserById(Integer id);
}
