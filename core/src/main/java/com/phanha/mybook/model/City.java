package com.phanha.mybook.model;

import javax.persistence.*;
import java.io.Serializable;

@Table(name="city")
@Entity
public class City  implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;


    @Column(name= "idcity")
    @Id
    @GeneratedValue (strategy = GenerationType.AUTO)
    private Integer idCity;

    @Column(name= "bzch")
    private String bzch;

    public Integer getIdCity() {
        return idCity;
    }

    public void setIdCity(Integer idCity) {
        this.idCity = idCity;
    }

    public String getBzch() {
        return bzch;
    }

    public void setBzch(String bzch) {
        this.bzch = bzch;
    }
}
