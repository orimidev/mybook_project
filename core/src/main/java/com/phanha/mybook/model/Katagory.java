package com.phanha.mybook.model;

import javax.persistence.*;
import java.io.Serializable;

@Table(name="category")
@Entity
public class Katagory implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;


    @Column(name= "idcategory")
    @Id
    @GeneratedValue (strategy = GenerationType.AUTO)
    private Integer idcatagory;

    @Column(name= "bzch")
    private String bzch;


    public Integer getIdcatagory() {
        return idcatagory;
    }

    public void setIdcatagory(Integer idcatagory) {
        this.idcatagory = idcatagory;
    }

    public String getBzch() {
        return bzch;
    }

    public void setBzch(String bzch) {
        this.bzch = bzch;
    }
}
