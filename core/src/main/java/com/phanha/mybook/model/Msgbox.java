package com.phanha.mybook.model;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Table(name="msg_box")
@Entity
public class Msgbox implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Column(name= "MESSAGE_BOX_ID")
    @Id
    @GeneratedValue (strategy = GenerationType.AUTO)
    private Integer msgBoxId;

    @Column(name= "MESSAGE")
    private String msg;

    @Column(name= "TIMESTAMP")
    private Timestamp timestamp;

    @Column(name= "NEW_MSG_FOR_SELLER")
    private Boolean newmsgForSeller;

    @Column(name= "NEW_MSG_FOR_BUYER")
    private String newmsgForBuyer;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "bookId")
    private Book book;


    @OneToOne(fetch = FetchType.EAGER, cascade=CascadeType.MERGE)
    @JoinColumn(name = "SELLER_ID")
    private User seller;

    @OneToOne(fetch = FetchType.EAGER,   cascade=CascadeType.MERGE)
    @JoinColumn(name = "BUYER_ID")
    private User buyer;

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public Integer getMsgBoxId() {
        return msgBoxId;
    }

    public void setMsgBoxId(Integer msgBoxId) {
        this.msgBoxId = msgBoxId;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Boolean getNewmsgForSeller() {
        return newmsgForSeller;
    }

    public void setNewmsgForSeller(Boolean newmsgForSeller) {
        this.newmsgForSeller = newmsgForSeller;
    }

    public String getNewmsgForBuyer() {
        return newmsgForBuyer;
    }

    public void setNewmsgForBuyer(String newmsgForBuyer) {
        this.newmsgForBuyer = newmsgForBuyer;
    }

    public User getSeller() {
        return seller;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public void setSeller(User seller) {
        this.seller = seller;
    }

    public User getBuyer() {
        return buyer;
    }

    public void setBuyer(User buyer) {
        this.buyer = buyer;
    }
}
