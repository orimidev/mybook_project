package com.phanha.mybook.model;

import javax.persistence.*;
import java.io.Serializable;

@Table(name="adress")
@Entity
public class Adress  implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;


    @Column(name= "idadress")
    @Id
    @GeneratedValue (strategy = GenerationType.AUTO)
    private Integer adressId;

    @Column(name= "street")
    private String strasse;

    @Column(name= "plz")
    private int plz;

    @OneToOne(fetch = FetchType.EAGER, cascade=CascadeType.MERGE)
    @JoinColumn(name = "city", referencedColumnName = "idcity")
    private City city;

    public Integer getAdressId() {
        return adressId;
    }

    public void setAdressId(Integer adressId) {
        this.adressId = adressId;
    }

    public String getStrasse() {
        return strasse;
    }

    public void setStrasse(String strasse) {
        this.strasse = strasse;
    }

    public int getPlz() {
        return plz;
    }

    public void setPlz(int plz) {
        this.plz = plz;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }
}
