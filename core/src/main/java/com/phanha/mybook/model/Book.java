package com.phanha.mybook.model;


import javax.persistence.*;
import java.io.Serializable;

@Table(name="book")
@Entity
public class Book implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Column(name= "idbook")
    @Id
    @GeneratedValue (strategy = GenerationType.AUTO)
    private Integer bookid;

    @Column(name= "bzch")
    private String bzch;


    @Column(name= "author")
    private String author;

    @Column(name= "lat")
    private String lat;

    @Column(name= "log")
    private String lng;

    @Column(name="img")
    private byte[] img;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "old_owner")
    private User userOfBook;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "new_owner")
    private User byerOfBook;

    @Column(name="deskription")
    private String deskription;

    @OneToOne (fetch = FetchType.EAGER)
    @JoinColumn(name = "city")
    private City city;

    @Column(name= "plz")
    private Integer plz;


    @OneToOne (fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinColumn(name = "katagory")
    private Katagory katagory;

    @Column(name="aktiv")
    private Boolean aktiv;


    @Column (name="abhol_datum")
    private String abholDatum;

    public String getAbholDatum() {
        return abholDatum;
    }

    public void setAbholDatum(String abholDatum) {
        this.abholDatum = abholDatum;
    }

    public User getUserOfBook() {
        return userOfBook;
    }

    public void setUserOfBook(User userOfBook) {
        this.userOfBook = userOfBook;
    }

    public Integer getBookid() {
        return bookid;
    }

    public void setBookid(Integer bookid) {
        this.bookid = bookid;
    }

    public String getBzch() {
        return bzch;
    }

    public void setBzch(String bzch) {
        this.bzch = bzch;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public byte[] getImg() {
        return img;
    }

    public void setImg(byte[] img) {
        this.img = img;
    }

    public User getByerOfBook() {
        return byerOfBook;
    }

    public void setByerOfBook(User byerOfBook) {
        this.byerOfBook = byerOfBook;
    }

    public String getDeskription() {
        return deskription;
    }

    public void setDeskription(String deskription) {
        this.deskription = deskription;
    }

    public Katagory getKatagory() {
        return katagory;
    }

    public void setKatagory(Katagory katagory) {
        this.katagory = katagory;
    }

    public Boolean getAktiv() {
        return aktiv;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public Integer getPlz() {
        return plz;
    }

    public void setPlz(Integer plz) {
        this.plz = plz;
    }

    public void setAktiv(Boolean aktiv) {
        this.aktiv = aktiv;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }
}
