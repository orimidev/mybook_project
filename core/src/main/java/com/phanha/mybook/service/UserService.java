package com.phanha.mybook.service;

import com.phanha.mybook.model.User;
import com.phanha.mybook.repo.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    @Autowired
    private UserRepo repo;


    public List<User> getAllUsers (){
        return repo.findAll();
    }

    public User getUserByLoginData (String u, String p) {
        User user = repo.findUserByUseNameAndPassword(u,p);
        if(user == null) {
           User unknownUser =  new User();
            unknownUser.setFirstName("unknown");
           return unknownUser;
        } else {
            return user;
        }
    }
}
