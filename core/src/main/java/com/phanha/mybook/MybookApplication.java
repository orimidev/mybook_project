package com.phanha.mybook;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class MybookApplication {

    public static void main(String[] args) {
        SpringApplication.run(MybookApplication.class, args);
    }

}
