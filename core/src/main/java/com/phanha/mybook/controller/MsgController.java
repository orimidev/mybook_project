package com.phanha.mybook.controller;


import com.phanha.mybook.model.Msgbox;
import com.phanha.mybook.repo.MsgRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;


@RestController
@RequestMapping("/msg_box")
@CrossOrigin
public class MsgController {


    @Autowired
    private MsgRepo repo;

    @GetMapping("/user/{id}")
    public List<Msgbox> getAllMsgBoxes (@PathVariable Integer id) {
        return repo.findAllMsgForUser(id);
    }

    @GetMapping("/{id}")
    public Msgbox getMsgBoxById (@PathVariable Integer id) {
        return repo.findByMsgBoxId(id);
    }

    @Transactional
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public Msgbox postMsgBox(@RequestBody Msgbox msg) {
        boolean newMsg = false;
        Integer msgId = null;
        Msgbox msgOld = new Msgbox();
        if(msg.getMsgBoxId() == null) {
            newMsg = true;
        } else {
          msgId = msg.getMsgBoxId();
          msgOld = repo.findByMsgBoxId(msgId);
        }

        if(newMsg||!msgOld.getMsg().equals(msg.getMsg())) {
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            msg.setTimestamp(timestamp);
        }
        return repo.save(msg);
    }




    @Transactional
    @GetMapping("remove/{id}")
    public void removeMsgById (@PathVariable Integer id) {
        repo.deleteById(id);
    }
}
