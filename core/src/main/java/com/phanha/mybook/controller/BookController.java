package com.phanha.mybook.controller;
import com.phanha.mybook.model.Book;
import com.phanha.mybook.model.City;
import com.phanha.mybook.repo.BookRepo;
import com.phanha.mybook.repo.CityRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/books")
@CrossOrigin
public class BookController {


    @Autowired
    private BookRepo repo;

    @Autowired
    private CityRepo cityRepo;

    @RequestMapping("/list")
    public List<Book> getAllBooks() {
        return repo.findAll();
    }

    @RequestMapping("/city")
    public List<City> getAllCity() {
        return cityRepo.findAll();
    }

    @RequestMapping("/filter")
    public List<Book> getAllBooksWithFilter(@RequestParam String search)
    {
        return repo.findAllBooksByKey(search);
    }


    @GetMapping("/actual/{id}")
        public List<Book> getAllBooksNotFromUserWithId (@PathVariable Integer id) {
        return repo.findAllBooksNotFromThisUser(id);
    }
    @GetMapping("/user/{id}")
    public List<Book> getAllBooksFromUserWithId (@PathVariable Integer id) {
        return repo.findAllBooksFromThisUser(id);
    }

    @GetMapping("/{id}")
    public Book getBookById (@PathVariable Integer id) {
        return repo.findByBookid(id);
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public Book registerUser(@RequestBody Book book) {
        //wenn ein Abholdatum gesetzt wird => das Buch ist nun nicht mehr zu anbieten
      return repo.save(book);
    }

    @Transactional
    @GetMapping("remove/{id}")
    public void removeBookById (@PathVariable Integer id) {
        repo.deleteById(id);
    }
}
