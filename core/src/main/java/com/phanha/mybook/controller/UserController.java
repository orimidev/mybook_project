package com.phanha.mybook.controller;

import com.phanha.mybook.model.Adress;
import com.phanha.mybook.model.Book;
import com.phanha.mybook.model.User;
import com.phanha.mybook.repo.AdressRepo;
import com.phanha.mybook.repo.BookRepo;
import com.phanha.mybook.repo.UserRepo;
import com.phanha.mybook.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
@CrossOrigin
public class UserController {
    @Autowired
    private UserService service;

    @Autowired
    private UserRepo repo;

    @Autowired
    private AdressRepo adressRepo;

    @Autowired
    private BookRepo bookRepo;


    @RequestMapping("/list")
    public List<User> getAllUsers() {
        return service.getAllUsers();
    }

    @RequestMapping("/user")
    public User getUser(@RequestParam String usename, @RequestParam String password) {
        return service.getUserByLoginData(usename,password);
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public User registerUser(@RequestBody User user) {
        return repo.save(user);
    }

    @RequestMapping(value = "/changeAdress", method = RequestMethod.POST)
    public Adress changeAdress(@RequestBody Adress adress) {
        return adressRepo.save(adress);
    }


    @GetMapping("/{id}")
    public User getUserById (@PathVariable Integer id) {
        return repo.findUserById(id);
    }

    @GetMapping("/pickup/{id}")
    public List<Book> getAllBookToPickUp (@PathVariable Integer id) {
        return bookRepo.findAllBookToPickUpFromThisUsers(id);
    }

}
