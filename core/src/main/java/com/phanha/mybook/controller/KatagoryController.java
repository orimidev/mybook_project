package com.phanha.mybook.controller;

import com.phanha.mybook.model.Katagory;
import com.phanha.mybook.repo.KataRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/kata")
@CrossOrigin
public class KatagoryController {


    @Autowired
    private KataRepo repo;

    @RequestMapping("/list")
    public List<Katagory> getAllKatagory() {
        return repo.findAll();
    }


}
